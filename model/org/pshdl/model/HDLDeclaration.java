/*******************************************************************************
 * PSHDL is a library and (trans-)compiler for PSHDL input. It generates
 *     output suitable for implementation or simulation of it.
 *     
 *     Copyright (C) 2013 Karsten Becker (feedback (at) pshdl (dot) org)
 * 
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 *     This License does not grant permission to use the trade names, trademarks,
 *     service marks, or product names of the Licensor, except as required for 
 *     reasonable and customary use in describing the origin of the Work.
 * 
 * Contributors:
 *     Karsten Becker - initial API and implementation
 ******************************************************************************/
package org.pshdl.model;
import org.pshdl.model.*;
import org.pshdl.model.utils.*;
import org.pshdl.model.extensions.*;
import org.pshdl.model.validation.*;
import org.pshdl.model.validation.builtin.*;
import org.pshdl.model.validation.Problem.*;
import org.pshdl.model.utils.HDLQuery.HDLFieldAccess;
import org.pshdl.model.impl.*;
import com.google.common.base.*;
import com.google.common.collect.*;
import javax.annotation.*;
import java.util.concurrent.atomic.*;
import java.util.*;
import java.util.ArrayList;
import javax.annotation.Nullable;
import org.pshdl.model.impl.AbstractHDLDeclaration;



/**
 * The class HDLDeclaration contains the following fields
 * <ul>
 * <li>IHDLObject container. Can be <code>null</code>.</li>
 * <li>ArrayList<HDLAnnotation> annotations. Can be <code>null</code>.</li>
 * </ul>
*/
public abstract class HDLDeclaration extends AbstractHDLDeclaration  {
	/**
	 * Constructs a new instance of {@link HDLDeclaration}
	 * 
	 * @param container
	 *            the value for container. Can be <code>null</code>.
	 * @param annotations
	 *            the value for annotations. Can be <code>null</code>.
	 * @param validate
	 *			  if <code>true</code> the parameters will be validated.
	 */
	public HDLDeclaration(int id, @Nullable IHDLObject container, @Nullable Iterable<HDLAnnotation> annotations, boolean validate) {
		super(id, container, annotations, validate);
	}

	
	public HDLDeclaration() {
		super();
	}
	
	/**
	 * Returns the ClassType of this instance
	 */
	public HDLClass getClassType(){
		return HDLClass.HDLDeclaration;
	}
		/**
		 * The accessor for the field annotations which is of type ArrayList<HDLAnnotation>.
		 */
		public static HDLFieldAccess<HDLDeclaration, ArrayList<HDLAnnotation>> fAnnotations = new HDLFieldAccess<HDLDeclaration, ArrayList<HDLAnnotation>>("annotations"){
			public ArrayList<HDLAnnotation> getValue(HDLDeclaration obj){
				if (obj==null)
					return null;
				return obj.getAnnotations();
			}
			public HDLDeclaration setValue(HDLDeclaration obj, ArrayList<HDLAnnotation> value){
				if (obj==null)
					return null;
				return obj.setAnnotations(value);
			}
		};
//$CONTENT-BEGIN$
	public HDLAnnotation getAnnotation(Enum<?> range) {
		for (final HDLAnnotation anno : getAnnotations())
			if (anno.getName().equals(range.toString()))
				return anno;
		return null;
	}
//$CONTENT-END$
	
}
