/*******************************************************************************
 * PSHDL is a library and (trans-)compiler for PSHDL input. It generates
 *     output suitable for implementation or simulation of it.
 *     
 *     Copyright (C) 2013 Karsten Becker (feedback (at) pshdl (dot) org)
 * 
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 *     This License does not grant permission to use the trade names, trademarks,
 *     service marks, or product names of the Licensor, except as required for 
 *     reasonable and customary use in describing the origin of the Work.
 * 
 * Contributors:
 *     Karsten Becker - initial API and implementation
 ******************************************************************************/
package org.pshdl.model;
import org.pshdl.model.*;
import org.pshdl.model.utils.*;
import org.pshdl.model.extensions.*;
import org.pshdl.model.validation.*;
import org.pshdl.model.validation.builtin.*;
import org.pshdl.model.validation.Problem.*;
import org.pshdl.model.utils.HDLQuery.HDLFieldAccess;
import org.pshdl.model.impl.*;
import com.google.common.base.*;
import com.google.common.collect.*;
import javax.annotation.*;
import java.util.concurrent.atomic.*;
import java.util.*;



/**
 * The class HDLStatemachineInstantiation contains the following fields
 * <ul>
 * <li>IHDLObject container. Can be <code>null</code>.</li>
 * <li>HDLVariable var. Can <b>not</b> be <code>null</code>.</li>
 * <li>ArrayList<HDLArgument> arguments. Can be <code>null</code>.</li>
 * <li>HDLQualifiedName hSm. Can <b>not</b> be <code>null</code>.</li>
 * </ul>
*/
public class HDLStatemachineInstantiation extends AbstractHDLStatemachineInstantiation  {
	/**
	 * Constructs a new instance of {@link HDLStatemachineInstantiation}
	 * 
	 * @param container
	 *            the value for container. Can be <code>null</code>.
	 * @param var
	 *            the value for var. Can <b>not</b> be <code>null</code>.
	 * @param arguments
	 *            the value for arguments. Can be <code>null</code>.
	 * @param hSm
	 *            the value for hSm. Can <b>not</b> be <code>null</code>.
	 * @param validate
	 *			  if <code>true</code> the parameters will be validated.
	 */
	public HDLStatemachineInstantiation(int id, @Nullable IHDLObject container, @Nonnull HDLVariable var, @Nullable Iterable<HDLArgument> arguments, @Nonnull HDLQualifiedName hSm, boolean validate) {
		super(id, container, var, arguments, hSm, validate);
	}

	
	public HDLStatemachineInstantiation() {
		super();
	}
	
	/**
	 * Returns the ClassType of this instance
	 */
	public HDLClass getClassType(){
		return HDLClass.HDLStatemachineInstantiation;
	}
		/**
		 * The accessor for the field hSm which is of type HDLQualifiedName.
		 */
		public static HDLFieldAccess<HDLStatemachineInstantiation, HDLQualifiedName> fHSm = new HDLFieldAccess<HDLStatemachineInstantiation, HDLQualifiedName>("hSm"){
			public HDLQualifiedName getValue(HDLStatemachineInstantiation obj){
				if (obj==null)
					return null;
				return obj.getHSmRefName();
			}
			public HDLStatemachineInstantiation setValue(HDLStatemachineInstantiation obj, HDLQualifiedName value){
				if (obj==null)
					return null;
				return obj.setHSm(value);
			}
		};
//$CONTENT-BEGIN$
//$CONTENT-END$
	
}
