/*******************************************************************************
 * PSHDL is a library and (trans-)compiler for PSHDL input. It generates
 *     output suitable for implementation or simulation of it.
 *     
 *     Copyright (C) 2013 Karsten Becker (feedback (at) pshdl (dot) org)
 * 
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 *     This License does not grant permission to use the trade names, trademarks,
 *     service marks, or product names of the Licensor, except as required for 
 *     reasonable and customary use in describing the origin of the Work.
 * 
 * Contributors:
 *     Karsten Becker - initial API and implementation
 ******************************************************************************/
package org.pshdl.model;
import org.pshdl.model.*;
import org.pshdl.model.utils.*;
import org.pshdl.model.extensions.*;
import org.pshdl.model.validation.*;
import org.pshdl.model.validation.builtin.*;
import org.pshdl.model.validation.Problem.*;
import org.pshdl.model.utils.HDLQuery.HDLFieldAccess;
import org.pshdl.model.impl.*;
import com.google.common.base.*;
import com.google.common.collect.*;
import javax.annotation.*;
import java.util.concurrent.atomic.*;
import java.util.*;
import java.util.ArrayList;
import org.pshdl.model.HDLPrimitive.HDLPrimitiveType;
import org.pshdl.model.utils.HDLQualifiedName;
import javax.annotation.Nonnull;
import java.util.EnumSet;
import javax.annotation.Nullable;
import com.google.common.base.Optional;
import org.pshdl.model.impl.AbstractHDLVariableDeclaration;



/**
 * The class HDLVariableDeclaration contains the following fields
 * <ul>
 * <li>IHDLObject container. Can be <code>null</code>.</li>
 * <li>ArrayList<HDLAnnotation> annotations. Can be <code>null</code>.</li>
 * <li>HDLRegisterConfig register. Can be <code>null</code>.</li>
 * <li>HDLDirection direction. If <code>null</code>, {@link HDLDirection#INTERNAL} is used as default.</li>
 * <li>HDLQualifiedName type. Can <b>not</b> be <code>null</code>.</li>
 * <li>HDLPrimitive primitive. Can be <code>null</code>.</li>
 * <li>ArrayList<HDLVariable> variables. Can <b>not</b> be <code>null</code>, additionally the collection must contain at least one element.</li>
 * </ul>
*/
public class HDLVariableDeclaration extends AbstractHDLVariableDeclaration  {
	/**
	 * Constructs a new instance of {@link HDLVariableDeclaration}
	 * 
	 * @param container
	 *            the value for container. Can be <code>null</code>.
	 * @param annotations
	 *            the value for annotations. Can be <code>null</code>.
	 * @param register
	 *            the value for register. Can be <code>null</code>.
	 * @param direction
	 *            the value for direction. If <code>null</code>, {@link HDLDirection#INTERNAL} is used as default.
	 * @param type
	 *            the value for type. Can <b>not</b> be <code>null</code>.
	 * @param primitive
	 *            the value for primitive. Can be <code>null</code>.
	 * @param variables
	 *            the value for variables. Can <b>not</b> be <code>null</code>, additionally the collection must contain at least one element.
	 * @param validate
	 *			  if <code>true</code> the parameters will be validated.
	 */
	public HDLVariableDeclaration(int id, @Nullable IHDLObject container, @Nullable Iterable<HDLAnnotation> annotations, @Nullable HDLRegisterConfig register, @Nullable HDLDirection direction, @Nonnull HDLQualifiedName type, @Nullable HDLPrimitive primitive, @Nonnull Iterable<HDLVariable> variables, boolean validate) {
		super(id, container, annotations, register, direction, type, primitive, variables, validate);
	}

	
	public HDLVariableDeclaration() {
		super();
	}
	
	/**
	 * Returns the ClassType of this instance
	 */
	public HDLClass getClassType(){
		return HDLClass.HDLVariableDeclaration;
	}
	 public static enum HDLDirection {
	IN("in"), OUT("out"), INOUT("inout"), PARAMETER("param"), CONSTANT("const"), INTERNAL(""), HIDDEN("<HIDDEN>");	
		String str;
	
		HDLDirection(String op) {
			this.str = op;
		}
	
		@Nullable
		public static HDLDirection getOp(String op) {
			for (HDLDirection ass : values()) {
				if (ass.str.equals(op)) {
					return ass;
				}
			}
			return null;
		}
	
		@Override
		@Nonnull
		public String toString() {
			return str;
		}
	}
		/**
		 * The accessor for the field register which is of type HDLRegisterConfig.
		 */
		public static HDLFieldAccess<HDLVariableDeclaration, HDLRegisterConfig> fRegister = new HDLFieldAccess<HDLVariableDeclaration, HDLRegisterConfig>("register"){
			public HDLRegisterConfig getValue(HDLVariableDeclaration obj){
				if (obj==null)
					return null;
				return obj.getRegister();
			}
			public HDLVariableDeclaration setValue(HDLVariableDeclaration obj, HDLRegisterConfig value){
				if (obj==null)
					return null;
				return obj.setRegister(value);
			}
		};
		/**
		 * The accessor for the field direction which is of type HDLDirection.
		 */
		public static HDLFieldAccess<HDLVariableDeclaration, HDLDirection> fDirection = new HDLFieldAccess<HDLVariableDeclaration, HDLDirection>("direction"){
			public HDLDirection getValue(HDLVariableDeclaration obj){
				if (obj==null)
					return null;
				return obj.getDirection();
			}
			public HDLVariableDeclaration setValue(HDLVariableDeclaration obj, HDLDirection value){
				if (obj==null)
					return null;
				return obj.setDirection(value);
			}
		};
		/**
		 * The accessor for the field type which is of type HDLQualifiedName.
		 */
		public static HDLFieldAccess<HDLVariableDeclaration, HDLQualifiedName> fType = new HDLFieldAccess<HDLVariableDeclaration, HDLQualifiedName>("type"){
			public HDLQualifiedName getValue(HDLVariableDeclaration obj){
				if (obj==null)
					return null;
				return obj.getTypeRefName();
			}
			public HDLVariableDeclaration setValue(HDLVariableDeclaration obj, HDLQualifiedName value){
				if (obj==null)
					return null;
				return obj.setType(value);
			}
		};
		/**
		 * The accessor for the field primitive which is of type HDLPrimitive.
		 */
		public static HDLFieldAccess<HDLVariableDeclaration, HDLPrimitive> fPrimitive = new HDLFieldAccess<HDLVariableDeclaration, HDLPrimitive>("primitive"){
			public HDLPrimitive getValue(HDLVariableDeclaration obj){
				if (obj==null)
					return null;
				return obj.getPrimitive();
			}
			public HDLVariableDeclaration setValue(HDLVariableDeclaration obj, HDLPrimitive value){
				if (obj==null)
					return null;
				return obj.setPrimitive(value);
			}
		};
		/**
		 * The accessor for the field variables which is of type ArrayList<HDLVariable>.
		 */
		public static HDLFieldAccess<HDLVariableDeclaration, ArrayList<HDLVariable>> fVariables = new HDLFieldAccess<HDLVariableDeclaration, ArrayList<HDLVariable>>("variables"){
			public ArrayList<HDLVariable> getValue(HDLVariableDeclaration obj){
				if (obj==null)
					return null;
				return obj.getVariables();
			}
			public HDLVariableDeclaration setValue(HDLVariableDeclaration obj, ArrayList<HDLVariable> value){
				if (obj==null)
					return null;
				return obj.setVariables(value);
			}
		};
//$CONTENT-BEGIN$

	@Override
	public Optional<? extends HDLType> resolveType() {
		if (getPrimitive() != null)
			return Optional.<HDLType> fromNullable(getPrimitive());
		return super.resolveType();
	}

	public HDLVariableDeclaration setType(HDLType resolveType) {
		final HDLVariableDeclaration setType = super.setType(resolveType.asRef());
		if (resolveType instanceof HDLPrimitive) {
			final HDLPrimitive prim = (HDLPrimitive) resolveType;
			return setType.setPrimitive(prim);
		}
		return setType;
	}

	private static final EnumSet<HDLDirection> external = EnumSet.of(HDLDirection.IN, HDLDirection.OUT, HDLDirection.INOUT, HDLDirection.PARAMETER);

	public boolean isExternal() {
		return external.contains(getDirection());
	}
	
	public HDLVariableDeclaration setDirection(HDLFunctionParameter.RWType rw) {		
	switch (rw) {
		case WRITE:
		case RETURN: 
			return setDirection(HDLDirection.OUT);
		case READ: 
			return setDirection(HDLDirection.IN);
		case READWRITE:
			return setDirection(HDLDirection.INOUT);
		default: 
			return this;
		}
	}
	
	public HDLVariableDeclaration getInverseDirected() {
		HDLVariableDeclaration res = this.copyFiltered(CopyFilter.DEEP_META);
		//res = res.setAnnotations(new ArrayList<HDLAnnotation>());
		switch (direction) {
		case IN: 
			return res.setDirection(HDLDirection.OUT);
		case OUT: 
			return res.setDirection(HDLDirection.IN);
		default: 
			return res;
		}
	}
	
	public HDLVariableDeclaration addNamePrefixToVariables(String pref) {
		HDLVariableDeclaration res = this.copyFiltered(CopyFilter.DEEP_META);
		List<HDLVariable> varList = new ArrayList<HDLVariable>();
		for (HDLVariable var : res.getVariables()) {
			varList.add(var.setName(pref+var.getName()));
		}
		res = res.setVariables(varList);
		return res;
		
	}
	
	public static HDLVariableDeclaration getNewPrimitive(HDLDirection dir, HDLPrimitiveType type, String name, int width ) {
		HDLPrimitive prim;
		switch (type) {
		case INT:
			prim = HDLPrimitive.getInt();
			break;
		case INTEGER: 
			prim = HDLPrimitive.getInteger();
			break;
		case UINT: 
			prim = HDLPrimitive.getUint();
			break;
		case NATURAL: 
			prim = HDLPrimitive.getNatural();
			break;
		case BIT: 
			prim = HDLPrimitive.getBit();
			break;
		case BITVECTOR: 
			prim = HDLPrimitive.getBitvector();
			break;
		default: 	
			prim = HDLPrimitive.getBit();
		}
		HDLVariableDeclaration varDecl = new HDLVariableDeclaration(); 
		varDecl = varDecl.setDirection(dir);
		varDecl = varDecl.setPrimitive(prim);
		varDecl = varDecl.setType(new HDLQualifiedName(prim.getName()));
		varDecl = varDecl.addVariables(new HDLVariable().setName(name));
		return varDecl;
	}

//$CONTENT-END$
	
}
