/*******************************************************************************
 * PSHDL is a library and (trans-)compiler for PSHDL input. It generates
 *     output suitable for implementation or simulation of it.
 *     
 *     Copyright (C) 2013 Karsten Becker (feedback (at) pshdl (dot) org)
 * 
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 *     This License does not grant permission to use the trade names, trademarks,
 *     service marks, or product names of the Licensor, except as required for 
 *     reasonable and customary use in describing the origin of the Work.
 * 
 * Contributors:
 *     Karsten Becker - initial API and implementation
 ******************************************************************************/
package org.pshdl.model.impl;

import org.pshdl.model.*;
import org.pshdl.model.utils.*;
import org.pshdl.model.extensions.*;
import org.pshdl.model.validation.*;
import org.pshdl.model.validation.builtin.*;
import org.pshdl.model.validation.Problem.*;
import org.pshdl.model.utils.HDLQuery.HDLFieldAccess;
import org.pshdl.model.impl.*;
import com.google.common.base.*;
import com.google.common.collect.*;
import javax.annotation.*;
import java.util.concurrent.atomic.*;
import java.util.*;

@SuppressWarnings("all")
public abstract class AbstractHDLStatemachineElement extends HDLObject   implements HDLStatement{
	/**
	 * Constructs a new instance of {@link AbstractHDLStatemachineElement}
	 * 
	 * @param container
	 *            the value for container. Can be <code>null</code>.
	 * @param statements
	 *            the value for statements. Can be <code>null</code>.
	 * @param state
	 *            the value for state. Can be <code>null</code>.
	 * @param validate
	 *			  if <code>true</code> the parameters will be validated.
	 */
	public AbstractHDLStatemachineElement(int id, @Nullable IHDLObject container, @Nullable HDLStatement statements, @Nullable HDLState state, boolean validate) {
		super(id, container, validate);
		if (validate)
			statements=validateStatements(statements);
		if (statements!=null)
			this.statements=(HDLStatement)statements;
		else
			this.statements=null;
		if (validate)
			state=validateState(state);
		if (state!=null)
			this.state=(HDLState)state;
		else
			this.state=null;
	}

	public AbstractHDLStatemachineElement() {
		super();
		this.statements=null;
		this.state=null;
	}
protected final HDLStatement statements;
	/**
	 * Get the statements field. Can be <code>null</code>.
	 * 
	 * @return the field
	 */
	@Nullable
	public HDLStatement getStatements(){
		return statements;
	}
	protected HDLStatement validateStatements(HDLStatement statements){
		return statements;
		}
protected final HDLState state;
	/**
	 * Get the state field. Can be <code>null</code>.
	 * 
	 * @return the field
	 */
	@Nullable
	public HDLState getState(){
		return state;
	}
	protected HDLState validateState(HDLState state){
		return state;
		}
	/**
	 * Creates a copy of this class with the same fields.
	 * 
	 * @return a new instance of this class.
	 */
	@Nonnull
	public HDLStatemachineElement copy(){
		HDLStatemachineElement newObject=new HDLStatemachineElement(id, null, statements, state, false);
		copyMetaData(this, newObject, false);
		return newObject;
	}
	
	/**
	 * Creates a copy of this class with the same fields.
	 * 
	 * @return a new instance of this class.
	 */
	@Nonnull
	public HDLStatemachineElement copyFiltered(CopyFilter filter){
		HDLStatement filteredstatements=filter.copyObject("statements", this, statements);
		HDLState filteredstate=filter.copyObject("state", this, state);
		return filter.postFilter((HDLStatemachineElement)this, new HDLStatemachineElement(id, null, filteredstatements, filteredstate, false));
	}
	
	/**
	 * Creates a deep copy of this class with the same fields and freezes it.
	 * 
	 * @return a new instance of this class.
	 */
	@Nonnull
	public HDLStatemachineElement copyDeepFrozen(IHDLObject container){
		HDLStatemachineElement copy = copyFiltered(CopyFilter.DEEP_META);
		copy.freeze(container);
		return copy;
	}
	/**
	 * Setter for the field {@link #getContainer()}.
	 * 
	 * @param container
	 *            sets the new container of this object. Can be <code>null</code>.
	 * @return 
	 *			  the same instance of {@link HDLStatemachineElement} with the updated container field.
	 */
	@Nonnull
	public HDLStatemachineElement setContainer(@Nullable IHDLObject container){
		return (HDLStatemachineElement)super.setContainer(container);
	}
	/**
	 * Setter for the field {@link #getStatements()}.
	 * 
	 * @param statements
	 *            sets the new statements of this object. Can be <code>null</code>.
	 * @return 
	 *			  a new instance of {@link HDLStatemachineElement} with the updated statements field.
	 */
	@Nonnull
	public HDLStatemachineElement setStatements(@Nullable HDLStatement statements){
		statements=validateStatements(statements);
		HDLStatemachineElement res=new HDLStatemachineElement(id, container, statements, state, false);
		return res;
	}
	/**
	 * Setter for the field {@link #getState()}.
	 * 
	 * @param state
	 *            sets the new state of this object. Can be <code>null</code>.
	 * @return 
	 *			  a new instance of {@link HDLStatemachineElement} with the updated state field.
	 */
	@Nonnull
	public HDLStatemachineElement setState(@Nullable HDLState state){
		state=validateState(state);
		HDLStatemachineElement res=new HDLStatemachineElement(id, container, statements, state, false);
		return res;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof AbstractHDLStatemachineElement))
			return false;
		if (!super.equals(obj))
			return false;
		AbstractHDLStatemachineElement other = (AbstractHDLStatemachineElement) obj;
		if (statements == null) {
			if (other.statements != null)
				return false;
		} else if (!statements.equals(other.statements))
			return false;
		if (state == null) {
			if (other.state != null)
				return false;
		} else if (!state.equals(other.state))
			return false;
		return true;
	}
	private Integer hashCache;
	
	@Override
	public int hashCode() {
		if (hashCache!=null)
			return hashCache;
		int result = super.hashCode();
		final int prime = 31;
		result = prime * result + ((statements == null) ? 0 : statements.hashCode());
		result = prime * result + ((state == null) ? 0 : state.hashCode());
		hashCache=result;
		return result;
	}
	public String toConstructionString(String spacing) {
		boolean first=true;
		StringBuilder sb=new StringBuilder();
		sb.append('\n').append(spacing).append("new HDLStatemachineElement()");
		if (statements!=null){
			sb.append(".setStatements(").append(statements.toConstructionString(spacing+"\t")).append(")");
		}
		if (state!=null){
			sb.append(".setState(").append(state.toConstructionString(spacing+"\t")).append(")");
		}
		return sb.toString();
	}
	public void validateAllFields(IHDLObject expectedParent, boolean checkResolve){
		super.validateAllFields(expectedParent, checkResolve);
		validateStatements(getStatements());
		if (getStatements()!=null){
			getStatements().validateAllFields(this, checkResolve);
		}
		validateState(getState());
		if (getState()!=null){
			getState().validateAllFields(this, checkResolve);
		}
		}
	public EnumSet<HDLClass> getClassSet(){
		return EnumSet.of(HDLClass.HDLStatemachineElement,HDLClass.HDLStatement,HDLClass.HDLObject);
	}
	public Iterator<IHDLObject> deepIterator() {
	return new Iterator<IHDLObject>() {

	private int pos = 0;
	private Iterator<? extends IHDLObject> current;

		@Override
		public boolean hasNext() {
	if ((current != null) && !current.hasNext())
		current = null;
	while (current == null) {
		switch (pos++) {
					case 0:
						if (statements!=null)
							current=Iterators.concat(Iterators.forArray(statements), statements.deepIterator());
						break;
					case 1:
						if (state!=null)
							current=Iterators.concat(Iterators.forArray(state), state.deepIterator());
						break;
				default:
	return false;
				}
			}
			return (current != null) && current.hasNext();
		}

		@Override
		public IHDLObject next() {
	return current.next();
		}

		@Override
		public void remove() {
	throw new IllegalArgumentException("Not supported");
		}

	};
}
	public Iterator<IHDLObject> iterator() {
	return new Iterator<IHDLObject>() {

	private int pos = 0;
	private Iterator<? extends IHDLObject> current;

		@Override
		public boolean hasNext() {
	if ((current != null) && !current.hasNext())
		current = null;
	while (current == null) {
		switch (pos++) {
					case 0:
						if (statements!=null)
							current=Iterators.singletonIterator(statements);
						break;
					case 1:
						if (state!=null)
							current=Iterators.singletonIterator(state);
						break;
				default:
	return false;
				}
			}
			return (current != null) && current.hasNext();
		}

		@Override
		public IHDLObject next() {
	return current.next();
		}

		@Override
		public void remove() {
	throw new IllegalArgumentException("Not supported");
		}

	};
}
}