/*******************************************************************************
 * PSHDL is a library and (trans-)compiler for PSHDL input. It generates
 *     output suitable for implementation or simulation of it.
 *     
 *     Copyright (C) 2013 Karsten Becker (feedback (at) pshdl (dot) org)
 * 
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 *     This License does not grant permission to use the trade names, trademarks,
 *     service marks, or product names of the Licensor, except as required for 
 *     reasonable and customary use in describing the origin of the Work.
 * 
 * Contributors:
 *     Karsten Becker - initial API and implementation
 ******************************************************************************/
package org.pshdl.model.impl;

import org.pshdl.model.*;
import org.pshdl.model.utils.*;
import org.pshdl.model.extensions.*;
import org.pshdl.model.validation.*;
import org.pshdl.model.validation.builtin.*;
import org.pshdl.model.validation.Problem.*;
import org.pshdl.model.utils.HDLQuery.HDLFieldAccess;
import org.pshdl.model.impl.*;
import com.google.common.base.*;
import com.google.common.collect.*;
import javax.annotation.*;
import java.util.concurrent.atomic.*;
import java.util.*;

@SuppressWarnings("all")
public abstract class AbstractHDLStatemachine extends HDLDeclaration  {
	/**
	 * Constructs a new instance of {@link AbstractHDLStatemachine}
	 * 
	 * @param container
	 *            the value for container. Can be <code>null</code>.
	 * @param annotations
	 *            the value for annotations. Can be <code>null</code>.
	 * @param imports
	 *            the value for imports. Can be <code>null</code>.
	 * @param inline
	 *            the value for inline. Can <b>not</b> be <code>null</code>.
	 * @param name
	 *            the value for name. Can <b>not</b> be <code>null</code>.
	 * @param returnType
	 *            the value for returnType. Can be <code>null</code>.
	 * @param args
	 *            the value for args. Can be <code>null</code>.
	 * @param elem
	 *            the value for elem. Can be <code>null</code>.
	 * @param validate
	 *			  if <code>true</code> the parameters will be validated.
	 */
	public AbstractHDLStatemachine(int id, @Nullable IHDLObject container, @Nullable Iterable<HDLAnnotation> annotations, @Nullable Iterable<String> imports, @Nonnull Boolean inline, @Nonnull String name, @Nullable HDLFunctionParameter returnType, @Nullable Iterable<HDLFunctionParameter> args, @Nullable Iterable<HDLStatemachineElement> elem, boolean validate) {
		super(id, container, annotations, validate);
		if (validate)
			imports=validateImports(imports);
		this.imports=new ArrayList<String>();
		if (imports!=null){
			for(String newValue:imports){
				this.imports.add(newValue);
			}
		}
		if (validate)
			inline=validateInline(inline);
		this.inline=inline;
		if (validate)
			name=validateName(name);
		this.name=name;
		if (validate)
			returnType=validateReturnType(returnType);
		if (returnType!=null)
			this.returnType=(HDLFunctionParameter)returnType;
		else
			this.returnType=null;
		if (validate)
			args=validateArgs(args);
		this.args=new ArrayList<HDLFunctionParameter>();
		if (args!=null){
			for(HDLFunctionParameter newValue:args){
				this.args.add((HDLFunctionParameter)newValue);
			}
		}
		if (validate)
			elem=validateElem(elem);
		this.elem=new ArrayList<HDLStatemachineElement>();
		if (elem!=null){
			for(HDLStatemachineElement newValue:elem){
				this.elem.add((HDLStatemachineElement)newValue);
			}
		}
	}

	public AbstractHDLStatemachine() {
		super();
		this.imports=new ArrayList<String>();
		this.inline=null;
		this.name=null;
		this.returnType=null;
		this.args=new ArrayList<HDLFunctionParameter>();
		this.elem=new ArrayList<HDLStatemachineElement>();
	}
protected final ArrayList<String> imports;
	/**
	 * Get the imports field. Can be <code>null</code>.
	 * 
	 * @return a clone of the field. Will never return <code>null</code>.
	 */
	@Nonnull
	public ArrayList<String> getImports(){
		return (ArrayList<String>) imports.clone();
	}
	protected Iterable<String> validateImports(Iterable<String> imports){
		if (imports==null)
			return new ArrayList<String>();
		return imports;
		}
protected final Boolean inline;
	/**
	 * Get the inline field. Can <b>not</b> be <code>null</code>.
	 * 
	 * @return the field
	 */
	@Nonnull
	public Boolean getInline(){
		return inline;
	}
	protected Boolean validateInline(Boolean inline){
		if (inline==null)
			throw new IllegalArgumentException("The field inline can not be null!");
		return inline;
		}
protected final String name;
	/**
	 * Get the name field. Can <b>not</b> be <code>null</code>.
	 * 
	 * @return the field
	 */
	@Nonnull
	public String getName(){
		return name;
	}
	protected String validateName(String name){
		if (name==null)
			throw new IllegalArgumentException("The field name can not be null!");
		return name;
		}
protected final HDLFunctionParameter returnType;
	/**
	 * Get the returnType field. Can be <code>null</code>.
	 * 
	 * @return the field
	 */
	@Nullable
	public HDLFunctionParameter getReturnType(){
		return returnType;
	}
	protected HDLFunctionParameter validateReturnType(HDLFunctionParameter returnType){
		return returnType;
		}
protected final ArrayList<HDLFunctionParameter> args;
	/**
	 * Get the args field. Can be <code>null</code>.
	 * 
	 * @return a clone of the field. Will never return <code>null</code>.
	 */
	@Nonnull
	public ArrayList<HDLFunctionParameter> getArgs(){
		return (ArrayList<HDLFunctionParameter>) args.clone();
	}
	protected Iterable<HDLFunctionParameter> validateArgs(Iterable<HDLFunctionParameter> args){
		if (args==null)
			return new ArrayList<HDLFunctionParameter>();
		return args;
		}
protected final ArrayList<HDLStatemachineElement> elem;
	/**
	 * Get the elem field. Can be <code>null</code>.
	 * 
	 * @return a clone of the field. Will never return <code>null</code>.
	 */
	@Nonnull
	public ArrayList<HDLStatemachineElement> getElem(){
		return (ArrayList<HDLStatemachineElement>) elem.clone();
	}
	protected Iterable<HDLStatemachineElement> validateElem(Iterable<HDLStatemachineElement> elem){
		if (elem==null)
			return new ArrayList<HDLStatemachineElement>();
		return elem;
		}
	/**
	 * Creates a copy of this class with the same fields.
	 * 
	 * @return a new instance of this class.
	 */
	@Nonnull
	public HDLStatemachine copy(){
		HDLStatemachine newObject=new HDLStatemachine(id, null, annotations, imports, inline, name, returnType, args, elem, false);
		copyMetaData(this, newObject, false);
		return newObject;
	}
	
	/**
	 * Creates a copy of this class with the same fields.
	 * 
	 * @return a new instance of this class.
	 */
	@Nonnull
	public HDLStatemachine copyFiltered(CopyFilter filter){
		ArrayList<HDLAnnotation> filteredannotations=filter.copyContainer("annotations", this, annotations);
		ArrayList<String> filteredimports=filter.copyContainer("imports", this, imports);
		Boolean filteredinline=filter.copyObject("inline", this, inline);
		String filteredname=filter.copyObject("name", this, name);
		HDLFunctionParameter filteredreturnType=filter.copyObject("returnType", this, returnType);
		ArrayList<HDLFunctionParameter> filteredargs=filter.copyContainer("args", this, args);
		ArrayList<HDLStatemachineElement> filteredelem=filter.copyContainer("elem", this, elem);
		return filter.postFilter((HDLStatemachine)this, new HDLStatemachine(id, null, filteredannotations, filteredimports, filteredinline, filteredname, filteredreturnType, filteredargs, filteredelem, false));
	}
	
	/**
	 * Creates a deep copy of this class with the same fields and freezes it.
	 * 
	 * @return a new instance of this class.
	 */
	@Nonnull
	public HDLStatemachine copyDeepFrozen(IHDLObject container){
		HDLStatemachine copy = copyFiltered(CopyFilter.DEEP_META);
		copy.freeze(container);
		return copy;
	}
	/**
	 * Setter for the field {@link #getContainer()}.
	 * 
	 * @param container
	 *            sets the new container of this object. Can be <code>null</code>.
	 * @return 
	 *			  the same instance of {@link HDLStatemachine} with the updated container field.
	 */
	@Nonnull
	public HDLStatemachine setContainer(@Nullable IHDLObject container){
		return (HDLStatemachine)super.setContainer(container);
	}
	/**
	 * Setter for the field {@link #getAnnotations()}.
	 * 
	 * @param annotations
	 *            sets the new annotations of this object. Can be <code>null</code>.
	 * @return 
	 *			  a new instance of {@link HDLStatemachine} with the updated annotations field.
	 */
	@Nonnull
	public HDLStatemachine setAnnotations(@Nullable Iterable<HDLAnnotation> annotations){
		annotations=validateAnnotations(annotations);
		HDLStatemachine res=new HDLStatemachine(id, container, annotations, imports, inline, name, returnType, args, elem, false);
		return res;
	}
	
	/**
	 * Adds a new value to the field {@link #getAnnotations()}.
	 * 
	 * @param newAnnotations
	 *            the value that should be added to the field {@link #getAnnotations()}
	 * @return a new instance of {@link HDLStatemachine} with the updated annotations field.
	 */
	@Nonnull
	public HDLStatemachine addAnnotations(@Nullable HDLAnnotation newAnnotations){
		if (newAnnotations == null)
			throw new IllegalArgumentException("Element of annotations can not be null!");
		ArrayList<HDLAnnotation> annotations=(ArrayList<HDLAnnotation>)this.annotations.clone();
		annotations.add(newAnnotations);
		HDLStatemachine res=new HDLStatemachine(id, container, annotations, imports, inline, name, returnType, args, elem, false);
		return res;
	}
	
	/**
	 * Removes a value from the field {@link #getAnnotations()}.
	 * 
	 * @param newAnnotations
	 *            the value that should be removed from the field {@link #getAnnotations()}
	 * @return a new instance of {@link HDLStatemachine} with the updated annotations field.
	 */
	@Nonnull
	public HDLStatemachine removeAnnotations(@Nullable HDLAnnotation newAnnotations){
		if (newAnnotations == null)
			throw new IllegalArgumentException("Removed element of annotations can not be null!");
		ArrayList<HDLAnnotation> annotations=(ArrayList<HDLAnnotation>)this.annotations.clone();
		annotations.remove(newAnnotations);
		HDLStatemachine res=new HDLStatemachine(id, container, annotations, imports, inline, name, returnType, args, elem, false);
		return res;
	}
	
	/**
	 * Removes a value from the field {@link #getAnnotations()}.
	 * 
	 * @param idx
	 *            the index of the value that should be removed from the field {@link #getAnnotations()}
	 * @return a new instance of {@link HDLStatemachine} with the updated annotations field.
	 */
	@Nonnull
	public HDLStatemachine removeAnnotations(int idx){
		ArrayList<HDLAnnotation> annotations=(ArrayList<HDLAnnotation>)this.annotations.clone();
		annotations.remove(idx);
		HDLStatemachine res=new HDLStatemachine(id, container, annotations, imports, inline, name, returnType, args, elem, false);
		return res;
	}	
	/**
	 * Setter for the field {@link #getImports()}.
	 * 
	 * @param imports
	 *            sets the new imports of this object. Can be <code>null</code>.
	 * @return 
	 *			  a new instance of {@link HDLStatemachine} with the updated imports field.
	 */
	@Nonnull
	public HDLStatemachine setImports(@Nullable Iterable<String> imports){
		imports=validateImports(imports);
		HDLStatemachine res=new HDLStatemachine(id, container, annotations, imports, inline, name, returnType, args, elem, false);
		return res;
	}
	
	/**
	 * Adds a new value to the field {@link #getImports()}.
	 * 
	 * @param newImports
	 *            the value that should be added to the field {@link #getImports()}
	 * @return a new instance of {@link HDLStatemachine} with the updated imports field.
	 */
	@Nonnull
	public HDLStatemachine addImports(@Nullable String newImports){
		if (newImports == null)
			throw new IllegalArgumentException("Element of imports can not be null!");
		ArrayList<String> imports=(ArrayList<String>)this.imports.clone();
		imports.add(newImports);
		HDLStatemachine res=new HDLStatemachine(id, container, annotations, imports, inline, name, returnType, args, elem, false);
		return res;
	}
	
	/**
	 * Removes a value from the field {@link #getImports()}.
	 * 
	 * @param newImports
	 *            the value that should be removed from the field {@link #getImports()}
	 * @return a new instance of {@link HDLStatemachine} with the updated imports field.
	 */
	@Nonnull
	public HDLStatemachine removeImports(@Nullable String newImports){
		if (newImports == null)
			throw new IllegalArgumentException("Removed element of imports can not be null!");
		ArrayList<String> imports=(ArrayList<String>)this.imports.clone();
		imports.remove(newImports);
		HDLStatemachine res=new HDLStatemachine(id, container, annotations, imports, inline, name, returnType, args, elem, false);
		return res;
	}
	
	/**
	 * Removes a value from the field {@link #getImports()}.
	 * 
	 * @param idx
	 *            the index of the value that should be removed from the field {@link #getImports()}
	 * @return a new instance of {@link HDLStatemachine} with the updated imports field.
	 */
	@Nonnull
	public HDLStatemachine removeImports(int idx){
		ArrayList<String> imports=(ArrayList<String>)this.imports.clone();
		imports.remove(idx);
		HDLStatemachine res=new HDLStatemachine(id, container, annotations, imports, inline, name, returnType, args, elem, false);
		return res;
	}	
	/**
	 * Setter for the field {@link #getInline()}.
	 * 
	 * @param inline
	 *            sets the new inline of this object. Can <b>not</b> be <code>null</code>.
	 * @return 
	 *			  a new instance of {@link HDLStatemachine} with the updated inline field.
	 */
	@Nonnull
	public HDLStatemachine setInline(@Nonnull Boolean inline){
		inline=validateInline(inline);
		HDLStatemachine res=new HDLStatemachine(id, container, annotations, imports, inline, name, returnType, args, elem, false);
		return res;
	}
	/**
	 * Setter for the field {@link #getInline()}.
	 * 
	 * @param inline
	 *            sets the new inline of this object. Can <b>not</b> be <code>null</code>.
	 * @return 
	 *			  a new instance of {@link HDLStatemachine} with the updated inline field.
	 */
	@Nonnull
	public HDLStatemachine setInline(boolean inline){
		inline=validateInline(inline);
		HDLStatemachine res=new HDLStatemachine(id, container, annotations, imports, inline, name, returnType, args, elem, false);
		return res;
	}
	/**
	 * Setter for the field {@link #getName()}.
	 * 
	 * @param name
	 *            sets the new name of this object. Can <b>not</b> be <code>null</code>.
	 * @return 
	 *			  a new instance of {@link HDLStatemachine} with the updated name field.
	 */
	@Nonnull
	public HDLStatemachine setName(@Nonnull String name){
		name=validateName(name);
		HDLStatemachine res=new HDLStatemachine(id, container, annotations, imports, inline, name, returnType, args, elem, false);
		return res;
	}
	/**
	 * Setter for the field {@link #getReturnType()}.
	 * 
	 * @param returnType
	 *            sets the new returnType of this object. Can be <code>null</code>.
	 * @return 
	 *			  a new instance of {@link HDLStatemachine} with the updated returnType field.
	 */
	@Nonnull
	public HDLStatemachine setReturnType(@Nullable HDLFunctionParameter returnType){
		returnType=validateReturnType(returnType);
		HDLStatemachine res=new HDLStatemachine(id, container, annotations, imports, inline, name, returnType, args, elem, false);
		return res;
	}
	/**
	 * Setter for the field {@link #getArgs()}.
	 * 
	 * @param args
	 *            sets the new args of this object. Can be <code>null</code>.
	 * @return 
	 *			  a new instance of {@link HDLStatemachine} with the updated args field.
	 */
	@Nonnull
	public HDLStatemachine setArgs(@Nullable Iterable<HDLFunctionParameter> args){
		args=validateArgs(args);
		HDLStatemachine res=new HDLStatemachine(id, container, annotations, imports, inline, name, returnType, args, elem, false);
		return res;
	}
	
	/**
	 * Adds a new value to the field {@link #getArgs()}.
	 * 
	 * @param newArgs
	 *            the value that should be added to the field {@link #getArgs()}
	 * @return a new instance of {@link HDLStatemachine} with the updated args field.
	 */
	@Nonnull
	public HDLStatemachine addArgs(@Nullable HDLFunctionParameter newArgs){
		if (newArgs == null)
			throw new IllegalArgumentException("Element of args can not be null!");
		ArrayList<HDLFunctionParameter> args=(ArrayList<HDLFunctionParameter>)this.args.clone();
		args.add(newArgs);
		HDLStatemachine res=new HDLStatemachine(id, container, annotations, imports, inline, name, returnType, args, elem, false);
		return res;
	}
	
	/**
	 * Removes a value from the field {@link #getArgs()}.
	 * 
	 * @param newArgs
	 *            the value that should be removed from the field {@link #getArgs()}
	 * @return a new instance of {@link HDLStatemachine} with the updated args field.
	 */
	@Nonnull
	public HDLStatemachine removeArgs(@Nullable HDLFunctionParameter newArgs){
		if (newArgs == null)
			throw new IllegalArgumentException("Removed element of args can not be null!");
		ArrayList<HDLFunctionParameter> args=(ArrayList<HDLFunctionParameter>)this.args.clone();
		args.remove(newArgs);
		HDLStatemachine res=new HDLStatemachine(id, container, annotations, imports, inline, name, returnType, args, elem, false);
		return res;
	}
	
	/**
	 * Removes a value from the field {@link #getArgs()}.
	 * 
	 * @param idx
	 *            the index of the value that should be removed from the field {@link #getArgs()}
	 * @return a new instance of {@link HDLStatemachine} with the updated args field.
	 */
	@Nonnull
	public HDLStatemachine removeArgs(int idx){
		ArrayList<HDLFunctionParameter> args=(ArrayList<HDLFunctionParameter>)this.args.clone();
		args.remove(idx);
		HDLStatemachine res=new HDLStatemachine(id, container, annotations, imports, inline, name, returnType, args, elem, false);
		return res;
	}	
	/**
	 * Setter for the field {@link #getElem()}.
	 * 
	 * @param elem
	 *            sets the new elem of this object. Can be <code>null</code>.
	 * @return 
	 *			  a new instance of {@link HDLStatemachine} with the updated elem field.
	 */
	@Nonnull
	public HDLStatemachine setElem(@Nullable Iterable<HDLStatemachineElement> elem){
		elem=validateElem(elem);
		HDLStatemachine res=new HDLStatemachine(id, container, annotations, imports, inline, name, returnType, args, elem, false);
		return res;
	}
	
	/**
	 * Adds a new value to the field {@link #getElem()}.
	 * 
	 * @param newElem
	 *            the value that should be added to the field {@link #getElem()}
	 * @return a new instance of {@link HDLStatemachine} with the updated elem field.
	 */
	@Nonnull
	public HDLStatemachine addElem(@Nullable HDLStatemachineElement newElem){
		if (newElem == null)
			throw new IllegalArgumentException("Element of elem can not be null!");
		ArrayList<HDLStatemachineElement> elem=(ArrayList<HDLStatemachineElement>)this.elem.clone();
		elem.add(newElem);
		HDLStatemachine res=new HDLStatemachine(id, container, annotations, imports, inline, name, returnType, args, elem, false);
		return res;
	}
	
	/**
	 * Removes a value from the field {@link #getElem()}.
	 * 
	 * @param newElem
	 *            the value that should be removed from the field {@link #getElem()}
	 * @return a new instance of {@link HDLStatemachine} with the updated elem field.
	 */
	@Nonnull
	public HDLStatemachine removeElem(@Nullable HDLStatemachineElement newElem){
		if (newElem == null)
			throw new IllegalArgumentException("Removed element of elem can not be null!");
		ArrayList<HDLStatemachineElement> elem=(ArrayList<HDLStatemachineElement>)this.elem.clone();
		elem.remove(newElem);
		HDLStatemachine res=new HDLStatemachine(id, container, annotations, imports, inline, name, returnType, args, elem, false);
		return res;
	}
	
	/**
	 * Removes a value from the field {@link #getElem()}.
	 * 
	 * @param idx
	 *            the index of the value that should be removed from the field {@link #getElem()}
	 * @return a new instance of {@link HDLStatemachine} with the updated elem field.
	 */
	@Nonnull
	public HDLStatemachine removeElem(int idx){
		ArrayList<HDLStatemachineElement> elem=(ArrayList<HDLStatemachineElement>)this.elem.clone();
		elem.remove(idx);
		HDLStatemachine res=new HDLStatemachine(id, container, annotations, imports, inline, name, returnType, args, elem, false);
		return res;
	}	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof AbstractHDLStatemachine))
			return false;
		if (!super.equals(obj))
			return false;
		AbstractHDLStatemachine other = (AbstractHDLStatemachine) obj;
		if (imports == null) {
			if (other.imports != null)
				return false;
		} else if (!imports.equals(other.imports))
			return false;
		if (inline == null) {
			if (other.inline != null)
				return false;
		} else if (!inline.equals(other.inline))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (returnType == null) {
			if (other.returnType != null)
				return false;
		} else if (!returnType.equals(other.returnType))
			return false;
		if (args == null) {
			if (other.args != null)
				return false;
		} else if (!args.equals(other.args))
			return false;
		if (elem == null) {
			if (other.elem != null)
				return false;
		} else if (!elem.equals(other.elem))
			return false;
		return true;
	}
	private Integer hashCache;
	
	@Override
	public int hashCode() {
		if (hashCache!=null)
			return hashCache;
		int result = super.hashCode();
		final int prime = 31;
		result = prime * result + ((imports == null) ? 0 : imports.hashCode());
		result = prime * result + ((inline == null) ? 0 : inline.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((returnType == null) ? 0 : returnType.hashCode());
		result = prime * result + ((args == null) ? 0 : args.hashCode());
		result = prime * result + ((elem == null) ? 0 : elem.hashCode());
		hashCache=result;
		return result;
	}
	public String toConstructionString(String spacing) {
		boolean first=true;
		StringBuilder sb=new StringBuilder();
		sb.append('\n').append(spacing).append("new HDLStatemachine()");
		if (annotations!=null){
			if (annotations.size()>0){
				sb.append('\n').append(spacing);
				for(HDLAnnotation o:annotations){
					sb.append(".addAnnotations(").append(o.toConstructionString(spacing+"\t\t"));
					sb.append('\n').append(spacing).append(")");
				}
			}
		}
		if (imports!=null){
			if (imports.size()>0) {
				sb.append('\n').append(spacing);
				for(String o:imports){
					sb.append(".addImports(");
					sb.append(o);
					sb.append('\n').append(spacing).append(")");
				}
			}
		}
		if (inline!=null){
			sb.append(".setInline(").append(inline).append(")");
		}
		if (name!=null){
			sb.append(".setName(").append('"'+name+'"').append(")");
		}
		if (returnType!=null){
			sb.append(".setReturnType(").append(returnType.toConstructionString(spacing+"\t")).append(")");
		}
		if (args!=null){
			if (args.size()>0){
				sb.append('\n').append(spacing);
				for(HDLFunctionParameter o:args){
					sb.append(".addArgs(").append(o.toConstructionString(spacing+"\t\t"));
					sb.append('\n').append(spacing).append(")");
				}
			}
		}
		if (elem!=null){
			if (elem.size()>0){
				sb.append('\n').append(spacing);
				for(HDLStatemachineElement o:elem){
					sb.append(".addElem(").append(o.toConstructionString(spacing+"\t\t"));
					sb.append('\n').append(spacing).append(")");
				}
			}
		}
		return sb.toString();
	}
	public void validateAllFields(IHDLObject expectedParent, boolean checkResolve){
		super.validateAllFields(expectedParent, checkResolve);
		validateImports(getImports());
		validateInline(getInline());
		validateName(getName());
		validateReturnType(getReturnType());
		if (getReturnType()!=null){
			getReturnType().validateAllFields(this, checkResolve);
		}
		validateArgs(getArgs());
		if (getArgs()!=null){
			for(HDLFunctionParameter o:getArgs()){
				o.validateAllFields(this, checkResolve);
			}
		}
		validateElem(getElem());
		if (getElem()!=null){
			for(HDLStatemachineElement o:getElem()){
				o.validateAllFields(this, checkResolve);
			}
		}
		}
	public EnumSet<HDLClass> getClassSet(){
		return EnumSet.of(HDLClass.HDLStatemachine,HDLClass.HDLDeclaration,HDLClass.HDLStatement,HDLClass.HDLObject);
	}
	public Iterator<IHDLObject> deepIterator() {
	return new Iterator<IHDLObject>() {

	private int pos = 0;
	private Iterator<? extends IHDLObject> current;

		@Override
		public boolean hasNext() {
	if ((current != null) && !current.hasNext())
		current = null;
	while (current == null) {
		switch (pos++) {
					case 0:
						if ((annotations != null) && (annotations .size() != 0)){
							List<Iterator<? extends IHDLObject>> iters=Lists.newArrayListWithCapacity(annotations.size());
							for (HDLAnnotation o : annotations) {
								iters.add(Iterators.forArray(o));
								iters.add(o.deepIterator());
							}
							current=Iterators.concat(iters.iterator());
						}	
						break;
					case 1:
						if (returnType!=null)
							current=Iterators.concat(Iterators.forArray(returnType), returnType.deepIterator());
						break;
					case 2:
						if ((args != null) && (args .size() != 0)){
							List<Iterator<? extends IHDLObject>> iters=Lists.newArrayListWithCapacity(args.size());
							for (HDLFunctionParameter o : args) {
								iters.add(Iterators.forArray(o));
								iters.add(o.deepIterator());
							}
							current=Iterators.concat(iters.iterator());
						}	
						break;
					case 3:
						if ((elem != null) && (elem .size() != 0)){
							List<Iterator<? extends IHDLObject>> iters=Lists.newArrayListWithCapacity(elem.size());
							for (HDLStatemachineElement o : elem) {
								iters.add(Iterators.forArray(o));
								iters.add(o.deepIterator());
							}
							current=Iterators.concat(iters.iterator());
						}	
						break;
				default:
	return false;
				}
			}
			return (current != null) && current.hasNext();
		}

		@Override
		public IHDLObject next() {
	return current.next();
		}

		@Override
		public void remove() {
	throw new IllegalArgumentException("Not supported");
		}

	};
}
	public Iterator<IHDLObject> iterator() {
	return new Iterator<IHDLObject>() {

	private int pos = 0;
	private Iterator<? extends IHDLObject> current;

		@Override
		public boolean hasNext() {
	if ((current != null) && !current.hasNext())
		current = null;
	while (current == null) {
		switch (pos++) {
					case 0:
						if ((annotations != null) && (annotations .size() != 0))
							current = annotations.iterator();
						break;
					case 1:
						if (returnType!=null)
							current=Iterators.singletonIterator(returnType);
						break;
					case 2:
						if ((args != null) && (args .size() != 0))
							current = args.iterator();
						break;
					case 3:
						if ((elem != null) && (elem .size() != 0))
							current = elem.iterator();
						break;
				default:
	return false;
				}
			}
			return (current != null) && current.hasNext();
		}

		@Override
		public IHDLObject next() {
	return current.next();
		}

		@Override
		public void remove() {
	throw new IllegalArgumentException("Not supported");
		}

	};
}
}