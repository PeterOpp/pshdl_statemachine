/*******************************************************************************
 * PSHDL is a library and (trans-)compiler for PSHDL input. It generates
 *     output suitable for implementation or simulation of it.
 *     
 *     Copyright (C) 2013 Karsten Becker (feedback (at) pshdl (dot) org)
 * 
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 *     This License does not grant permission to use the trade names, trademarks,
 *     service marks, or product names of the Licensor, except as required for 
 *     reasonable and customary use in describing the origin of the Work.
 * 
 * Contributors:
 *     Karsten Becker - initial API and implementation
 ******************************************************************************/
package org.pshdl.model.impl;

import org.pshdl.model.*;
import org.pshdl.model.utils.*;
import org.pshdl.model.extensions.*;
import org.pshdl.model.validation.*;
import org.pshdl.model.validation.builtin.*;
import org.pshdl.model.validation.Problem.*;
import org.pshdl.model.utils.HDLQuery.HDLFieldAccess;
import org.pshdl.model.impl.*;
import com.google.common.base.*;
import com.google.common.collect.*;
import javax.annotation.*;
import java.util.concurrent.atomic.*;
import java.util.*;

@SuppressWarnings("all")
public abstract class AbstractHDLState extends HDLObject  {
	/**
	 * Constructs a new instance of {@link AbstractHDLState}
	 * 
	 * @param container
	 *            the value for container. Can be <code>null</code>.
	 * @param stateLabel
	 *            the value for stateLabel. Can be <code>null</code>.
	 * @param annotations
	 *            the value for annotations. Can be <code>null</code>.
	 * @param statement
	 *            the value for statement. Can be <code>null</code>.
	 * @param validate
	 *			  if <code>true</code> the parameters will be validated.
	 */
	public AbstractHDLState(int id, @Nullable IHDLObject container, @Nullable String stateLabel, @Nullable Iterable<HDLAnnotation> annotations, @Nullable Iterable<HDLStatement> statement, boolean validate) {
		super(id, container, validate);
		if (validate)
			stateLabel=validateStateLabel(stateLabel);
		this.stateLabel=stateLabel;
		if (validate)
			annotations=validateAnnotations(annotations);
		this.annotations=new ArrayList<HDLAnnotation>();
		if (annotations!=null){
			for(HDLAnnotation newValue:annotations){
				this.annotations.add((HDLAnnotation)newValue);
			}
		}
		if (validate)
			statement=validateStatement(statement);
		this.statement=new ArrayList<HDLStatement>();
		if (statement!=null){
			for(HDLStatement newValue:statement){
				this.statement.add((HDLStatement)newValue);
			}
		}
	}

	public AbstractHDLState() {
		super();
		this.stateLabel=null;
		this.annotations=new ArrayList<HDLAnnotation>();
		this.statement=new ArrayList<HDLStatement>();
	}
protected final String stateLabel;
	/**
	 * Get the stateLabel field. Can be <code>null</code>.
	 * 
	 * @return the field
	 */
	@Nullable
	public String getStateLabel(){
		return stateLabel;
	}
	protected String validateStateLabel(String stateLabel){
		return stateLabel;
		}
protected final ArrayList<HDLAnnotation> annotations;
	/**
	 * Get the annotations field. Can be <code>null</code>.
	 * 
	 * @return a clone of the field. Will never return <code>null</code>.
	 */
	@Nonnull
	public ArrayList<HDLAnnotation> getAnnotations(){
		return (ArrayList<HDLAnnotation>) annotations.clone();
	}
	protected Iterable<HDLAnnotation> validateAnnotations(Iterable<HDLAnnotation> annotations){
		if (annotations==null)
			return new ArrayList<HDLAnnotation>();
		return annotations;
		}
protected final ArrayList<HDLStatement> statement;
	/**
	 * Get the statement field. Can be <code>null</code>.
	 * 
	 * @return a clone of the field. Will never return <code>null</code>.
	 */
	@Nonnull
	public ArrayList<HDLStatement> getStatement(){
		return (ArrayList<HDLStatement>) statement.clone();
	}
	protected Iterable<HDLStatement> validateStatement(Iterable<HDLStatement> statement){
		if (statement==null)
			return new ArrayList<HDLStatement>();
		return statement;
		}
	/**
	 * Creates a copy of this class with the same fields.
	 * 
	 * @return a new instance of this class.
	 */
	@Nonnull
	public HDLState copy(){
		HDLState newObject=new HDLState(id, null, stateLabel, annotations, statement, false);
		copyMetaData(this, newObject, false);
		return newObject;
	}
	
	/**
	 * Creates a copy of this class with the same fields.
	 * 
	 * @return a new instance of this class.
	 */
	@Nonnull
	public HDLState copyFiltered(CopyFilter filter){
		String filteredstateLabel=filter.copyObject("stateLabel", this, stateLabel);
		ArrayList<HDLAnnotation> filteredannotations=filter.copyContainer("annotations", this, annotations);
		ArrayList<HDLStatement> filteredstatement=filter.copyContainer("statement", this, statement);
		return filter.postFilter((HDLState)this, new HDLState(id, null, filteredstateLabel, filteredannotations, filteredstatement, false));
	}
	
	/**
	 * Creates a deep copy of this class with the same fields and freezes it.
	 * 
	 * @return a new instance of this class.
	 */
	@Nonnull
	public HDLState copyDeepFrozen(IHDLObject container){
		HDLState copy = copyFiltered(CopyFilter.DEEP_META);
		copy.freeze(container);
		return copy;
	}
	/**
	 * Setter for the field {@link #getContainer()}.
	 * 
	 * @param container
	 *            sets the new container of this object. Can be <code>null</code>.
	 * @return 
	 *			  the same instance of {@link HDLState} with the updated container field.
	 */
	@Nonnull
	public HDLState setContainer(@Nullable IHDLObject container){
		return (HDLState)super.setContainer(container);
	}
	/**
	 * Setter for the field {@link #getStateLabel()}.
	 * 
	 * @param stateLabel
	 *            sets the new stateLabel of this object. Can be <code>null</code>.
	 * @return 
	 *			  a new instance of {@link HDLState} with the updated stateLabel field.
	 */
	@Nonnull
	public HDLState setStateLabel(@Nullable String stateLabel){
		stateLabel=validateStateLabel(stateLabel);
		HDLState res=new HDLState(id, container, stateLabel, annotations, statement, false);
		return res;
	}
	/**
	 * Setter for the field {@link #getAnnotations()}.
	 * 
	 * @param annotations
	 *            sets the new annotations of this object. Can be <code>null</code>.
	 * @return 
	 *			  a new instance of {@link HDLState} with the updated annotations field.
	 */
	@Nonnull
	public HDLState setAnnotations(@Nullable Iterable<HDLAnnotation> annotations){
		annotations=validateAnnotations(annotations);
		HDLState res=new HDLState(id, container, stateLabel, annotations, statement, false);
		return res;
	}
	
	/**
	 * Adds a new value to the field {@link #getAnnotations()}.
	 * 
	 * @param newAnnotations
	 *            the value that should be added to the field {@link #getAnnotations()}
	 * @return a new instance of {@link HDLState} with the updated annotations field.
	 */
	@Nonnull
	public HDLState addAnnotations(@Nullable HDLAnnotation newAnnotations){
		if (newAnnotations == null)
			throw new IllegalArgumentException("Element of annotations can not be null!");
		ArrayList<HDLAnnotation> annotations=(ArrayList<HDLAnnotation>)this.annotations.clone();
		annotations.add(newAnnotations);
		HDLState res=new HDLState(id, container, stateLabel, annotations, statement, false);
		return res;
	}
	
	/**
	 * Removes a value from the field {@link #getAnnotations()}.
	 * 
	 * @param newAnnotations
	 *            the value that should be removed from the field {@link #getAnnotations()}
	 * @return a new instance of {@link HDLState} with the updated annotations field.
	 */
	@Nonnull
	public HDLState removeAnnotations(@Nullable HDLAnnotation newAnnotations){
		if (newAnnotations == null)
			throw new IllegalArgumentException("Removed element of annotations can not be null!");
		ArrayList<HDLAnnotation> annotations=(ArrayList<HDLAnnotation>)this.annotations.clone();
		annotations.remove(newAnnotations);
		HDLState res=new HDLState(id, container, stateLabel, annotations, statement, false);
		return res;
	}
	
	/**
	 * Removes a value from the field {@link #getAnnotations()}.
	 * 
	 * @param idx
	 *            the index of the value that should be removed from the field {@link #getAnnotations()}
	 * @return a new instance of {@link HDLState} with the updated annotations field.
	 */
	@Nonnull
	public HDLState removeAnnotations(int idx){
		ArrayList<HDLAnnotation> annotations=(ArrayList<HDLAnnotation>)this.annotations.clone();
		annotations.remove(idx);
		HDLState res=new HDLState(id, container, stateLabel, annotations, statement, false);
		return res;
	}	
	/**
	 * Setter for the field {@link #getStatement()}.
	 * 
	 * @param statement
	 *            sets the new statement of this object. Can be <code>null</code>.
	 * @return 
	 *			  a new instance of {@link HDLState} with the updated statement field.
	 */
	@Nonnull
	public HDLState setStatement(@Nullable Iterable<HDLStatement> statement){
		statement=validateStatement(statement);
		HDLState res=new HDLState(id, container, stateLabel, annotations, statement, false);
		return res;
	}
	
	/**
	 * Adds a new value to the field {@link #getStatement()}.
	 * 
	 * @param newStatement
	 *            the value that should be added to the field {@link #getStatement()}
	 * @return a new instance of {@link HDLState} with the updated statement field.
	 */
	@Nonnull
	public HDLState addStatement(@Nullable HDLStatement newStatement){
		if (newStatement == null)
			throw new IllegalArgumentException("Element of statement can not be null!");
		ArrayList<HDLStatement> statement=(ArrayList<HDLStatement>)this.statement.clone();
		statement.add(newStatement);
		HDLState res=new HDLState(id, container, stateLabel, annotations, statement, false);
		return res;
	}
	
	/**
	 * Removes a value from the field {@link #getStatement()}.
	 * 
	 * @param newStatement
	 *            the value that should be removed from the field {@link #getStatement()}
	 * @return a new instance of {@link HDLState} with the updated statement field.
	 */
	@Nonnull
	public HDLState removeStatement(@Nullable HDLStatement newStatement){
		if (newStatement == null)
			throw new IllegalArgumentException("Removed element of statement can not be null!");
		ArrayList<HDLStatement> statement=(ArrayList<HDLStatement>)this.statement.clone();
		statement.remove(newStatement);
		HDLState res=new HDLState(id, container, stateLabel, annotations, statement, false);
		return res;
	}
	
	/**
	 * Removes a value from the field {@link #getStatement()}.
	 * 
	 * @param idx
	 *            the index of the value that should be removed from the field {@link #getStatement()}
	 * @return a new instance of {@link HDLState} with the updated statement field.
	 */
	@Nonnull
	public HDLState removeStatement(int idx){
		ArrayList<HDLStatement> statement=(ArrayList<HDLStatement>)this.statement.clone();
		statement.remove(idx);
		HDLState res=new HDLState(id, container, stateLabel, annotations, statement, false);
		return res;
	}	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof AbstractHDLState))
			return false;
		if (!super.equals(obj))
			return false;
		AbstractHDLState other = (AbstractHDLState) obj;
		if (stateLabel == null) {
			if (other.stateLabel != null)
				return false;
		} else if (!stateLabel.equals(other.stateLabel))
			return false;
		if (annotations == null) {
			if (other.annotations != null)
				return false;
		} else if (!annotations.equals(other.annotations))
			return false;
		if (statement == null) {
			if (other.statement != null)
				return false;
		} else if (!statement.equals(other.statement))
			return false;
		return true;
	}
	private Integer hashCache;
	
	@Override
	public int hashCode() {
		if (hashCache!=null)
			return hashCache;
		int result = super.hashCode();
		final int prime = 31;
		result = prime * result + ((stateLabel == null) ? 0 : stateLabel.hashCode());
		result = prime * result + ((annotations == null) ? 0 : annotations.hashCode());
		result = prime * result + ((statement == null) ? 0 : statement.hashCode());
		hashCache=result;
		return result;
	}
	public String toConstructionString(String spacing) {
		boolean first=true;
		StringBuilder sb=new StringBuilder();
		sb.append('\n').append(spacing).append("new HDLState()");
		if (stateLabel!=null){
			sb.append(".setStateLabel(").append('"'+stateLabel+'"').append(")");
		}
		if (annotations!=null){
			if (annotations.size()>0){
				sb.append('\n').append(spacing);
				for(HDLAnnotation o:annotations){
					sb.append(".addAnnotations(").append(o.toConstructionString(spacing+"\t\t"));
					sb.append('\n').append(spacing).append(")");
				}
			}
		}
		if (statement!=null){
			if (statement.size()>0){
				sb.append('\n').append(spacing);
				for(HDLStatement o:statement){
					sb.append(".addStatement(").append(o.toConstructionString(spacing+"\t\t"));
					sb.append('\n').append(spacing).append(")");
				}
			}
		}
		return sb.toString();
	}
	public void validateAllFields(IHDLObject expectedParent, boolean checkResolve){
		super.validateAllFields(expectedParent, checkResolve);
		validateStateLabel(getStateLabel());
		validateAnnotations(getAnnotations());
		if (getAnnotations()!=null){
			for(HDLAnnotation o:getAnnotations()){
				o.validateAllFields(this, checkResolve);
			}
		}
		validateStatement(getStatement());
		if (getStatement()!=null){
			for(HDLStatement o:getStatement()){
				o.validateAllFields(this, checkResolve);
			}
		}
		}
	public EnumSet<HDLClass> getClassSet(){
		return EnumSet.of(HDLClass.HDLState,HDLClass.HDLObject);
	}
	public Iterator<IHDLObject> deepIterator() {
	return new Iterator<IHDLObject>() {

	private int pos = 0;
	private Iterator<? extends IHDLObject> current;

		@Override
		public boolean hasNext() {
	if ((current != null) && !current.hasNext())
		current = null;
	while (current == null) {
		switch (pos++) {
					case 0:
						if ((annotations != null) && (annotations .size() != 0)){
							List<Iterator<? extends IHDLObject>> iters=Lists.newArrayListWithCapacity(annotations.size());
							for (HDLAnnotation o : annotations) {
								iters.add(Iterators.forArray(o));
								iters.add(o.deepIterator());
							}
							current=Iterators.concat(iters.iterator());
						}	
						break;
					case 1:
						if ((statement != null) && (statement .size() != 0)){
							List<Iterator<? extends IHDLObject>> iters=Lists.newArrayListWithCapacity(statement.size());
							for (HDLStatement o : statement) {
								iters.add(Iterators.forArray(o));
								iters.add(o.deepIterator());
							}
							current=Iterators.concat(iters.iterator());
						}	
						break;
				default:
	return false;
				}
			}
			return (current != null) && current.hasNext();
		}

		@Override
		public IHDLObject next() {
	return current.next();
		}

		@Override
		public void remove() {
	throw new IllegalArgumentException("Not supported");
		}

	};
}
	public Iterator<IHDLObject> iterator() {
	return new Iterator<IHDLObject>() {

	private int pos = 0;
	private Iterator<? extends IHDLObject> current;

		@Override
		public boolean hasNext() {
	if ((current != null) && !current.hasNext())
		current = null;
	while (current == null) {
		switch (pos++) {
					case 0:
						if ((annotations != null) && (annotations .size() != 0))
							current = annotations.iterator();
						break;
					case 1:
						if ((statement != null) && (statement .size() != 0))
							current = statement.iterator();
						break;
				default:
	return false;
				}
			}
			return (current != null) && current.hasNext();
		}

		@Override
		public IHDLObject next() {
	return current.next();
		}

		@Override
		public void remove() {
	throw new IllegalArgumentException("Not supported");
		}

	};
}
}