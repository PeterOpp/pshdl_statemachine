/*******************************************************************************
 * PSHDL is a library and (trans-)compiler for PSHDL input. It generates
 *     output suitable for implementation or simulation of it.
 *     
 *     Copyright (C) 2013 Karsten Becker (feedback (at) pshdl (dot) org)
 * 
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 *     This License does not grant permission to use the trade names, trademarks,
 *     service marks, or product names of the Licensor, except as required for 
 *     reasonable and customary use in describing the origin of the Work.
 * 
 * Contributors:
 *     Karsten Becker - initial API and implementation
 ******************************************************************************/
package org.pshdl.model.impl;

import org.pshdl.model.*;
import org.pshdl.model.utils.*;
import org.pshdl.model.extensions.*;
import org.pshdl.model.validation.*;
import org.pshdl.model.validation.builtin.*;
import org.pshdl.model.validation.Problem.*;
import org.pshdl.model.utils.HDLQuery.HDLFieldAccess;
import org.pshdl.model.impl.*;
import com.google.common.base.*;
import com.google.common.collect.*;
import javax.annotation.*;
import java.util.concurrent.atomic.*;
import java.util.*;

@SuppressWarnings("all")
public abstract class AbstractHDLStatemachineInterface extends HDLInterface  {
	/**
	 * Constructs a new instance of {@link AbstractHDLStatemachineInterface}
	 * 
	 * @param container
	 *            the value for container. Can be <code>null</code>.
	 * @param name
	 *            the value for name. Can <b>not</b> be <code>null</code>.
	 * @param dim
	 *            the value for dim. Can be <code>null</code>.
	 * @param ports
	 *            the value for ports. Can be <code>null</code>.
	 * @param validate
	 *			  if <code>true</code> the parameters will be validated.
	 */
	public AbstractHDLStatemachineInterface(int id, @Nullable IHDLObject container, @Nonnull String name, @Nullable Iterable<HDLExpression> dim, @Nullable Iterable<HDLVariableDeclaration> ports, boolean validate) {
		super(id, container, name, dim, ports, validate);
	}

	public AbstractHDLStatemachineInterface() {
		super();
	}
	/**
	 * Creates a copy of this class with the same fields.
	 * 
	 * @return a new instance of this class.
	 */
	@Nonnull
	public HDLStatemachineInterface copy(){
		HDLStatemachineInterface newObject=new HDLStatemachineInterface(id, null, name, dim, ports, false);
		copyMetaData(this, newObject, false);
		return newObject;
	}
	
	/**
	 * Creates a copy of this class with the same fields.
	 * 
	 * @return a new instance of this class.
	 */
	@Nonnull
	public HDLStatemachineInterface copyFiltered(CopyFilter filter){
		String filteredname=filter.copyObject("name", this, name);
		ArrayList<HDLExpression> filtereddim=filter.copyContainer("dim", this, dim);
		ArrayList<HDLVariableDeclaration> filteredports=filter.copyContainer("ports", this, ports);
		return filter.postFilter((HDLStatemachineInterface)this, new HDLStatemachineInterface(id, null, filteredname, filtereddim, filteredports, false));
	}
	
	/**
	 * Creates a deep copy of this class with the same fields and freezes it.
	 * 
	 * @return a new instance of this class.
	 */
	@Nonnull
	public HDLStatemachineInterface copyDeepFrozen(IHDLObject container){
		HDLStatemachineInterface copy = copyFiltered(CopyFilter.DEEP_META);
		copy.freeze(container);
		return copy;
	}
	/**
	 * Setter for the field {@link #getContainer()}.
	 * 
	 * @param container
	 *            sets the new container of this object. Can be <code>null</code>.
	 * @return 
	 *			  the same instance of {@link HDLStatemachineInterface} with the updated container field.
	 */
	@Nonnull
	public HDLStatemachineInterface setContainer(@Nullable IHDLObject container){
		return (HDLStatemachineInterface)super.setContainer(container);
	}
	/**
	 * Setter for the field {@link #getName()}.
	 * 
	 * @param name
	 *            sets the new name of this object. Can <b>not</b> be <code>null</code>.
	 * @return 
	 *			  a new instance of {@link HDLStatemachineInterface} with the updated name field.
	 */
	@Nonnull
	public HDLStatemachineInterface setName(@Nonnull String name){
		name=validateName(name);
		HDLStatemachineInterface res=new HDLStatemachineInterface(id, container, name, dim, ports, false);
		return res;
	}
	/**
	 * Setter for the field {@link #getDim()}.
	 * 
	 * @param dim
	 *            sets the new dim of this object. Can be <code>null</code>.
	 * @return 
	 *			  a new instance of {@link HDLStatemachineInterface} with the updated dim field.
	 */
	@Nonnull
	public HDLStatemachineInterface setDim(@Nullable Iterable<HDLExpression> dim){
		dim=validateDim(dim);
		HDLStatemachineInterface res=new HDLStatemachineInterface(id, container, name, dim, ports, false);
		return res;
	}
	
	/**
	 * Adds a new value to the field {@link #getDim()}.
	 * 
	 * @param newDim
	 *            the value that should be added to the field {@link #getDim()}
	 * @return a new instance of {@link HDLStatemachineInterface} with the updated dim field.
	 */
	@Nonnull
	public HDLStatemachineInterface addDim(@Nullable HDLExpression newDim){
		if (newDim == null)
			throw new IllegalArgumentException("Element of dim can not be null!");
		ArrayList<HDLExpression> dim=(ArrayList<HDLExpression>)this.dim.clone();
		dim.add(newDim);
		HDLStatemachineInterface res=new HDLStatemachineInterface(id, container, name, dim, ports, false);
		return res;
	}
	
	/**
	 * Removes a value from the field {@link #getDim()}.
	 * 
	 * @param newDim
	 *            the value that should be removed from the field {@link #getDim()}
	 * @return a new instance of {@link HDLStatemachineInterface} with the updated dim field.
	 */
	@Nonnull
	public HDLStatemachineInterface removeDim(@Nullable HDLExpression newDim){
		if (newDim == null)
			throw new IllegalArgumentException("Removed element of dim can not be null!");
		ArrayList<HDLExpression> dim=(ArrayList<HDLExpression>)this.dim.clone();
		dim.remove(newDim);
		HDLStatemachineInterface res=new HDLStatemachineInterface(id, container, name, dim, ports, false);
		return res;
	}
	
	/**
	 * Removes a value from the field {@link #getDim()}.
	 * 
	 * @param idx
	 *            the index of the value that should be removed from the field {@link #getDim()}
	 * @return a new instance of {@link HDLStatemachineInterface} with the updated dim field.
	 */
	@Nonnull
	public HDLStatemachineInterface removeDim(int idx){
		ArrayList<HDLExpression> dim=(ArrayList<HDLExpression>)this.dim.clone();
		dim.remove(idx);
		HDLStatemachineInterface res=new HDLStatemachineInterface(id, container, name, dim, ports, false);
		return res;
	}	
	/**
	 * Setter for the field {@link #getPorts()}.
	 * 
	 * @param ports
	 *            sets the new ports of this object. Can be <code>null</code>.
	 * @return 
	 *			  a new instance of {@link HDLStatemachineInterface} with the updated ports field.
	 */
	@Nonnull
	public HDLStatemachineInterface setPorts(@Nullable Iterable<HDLVariableDeclaration> ports){
		ports=validatePorts(ports);
		HDLStatemachineInterface res=new HDLStatemachineInterface(id, container, name, dim, ports, false);
		return res;
	}
	
	/**
	 * Adds a new value to the field {@link #getPorts()}.
	 * 
	 * @param newPorts
	 *            the value that should be added to the field {@link #getPorts()}
	 * @return a new instance of {@link HDLStatemachineInterface} with the updated ports field.
	 */
	@Nonnull
	public HDLStatemachineInterface addPorts(@Nullable HDLVariableDeclaration newPorts){
		if (newPorts == null)
			throw new IllegalArgumentException("Element of ports can not be null!");
		ArrayList<HDLVariableDeclaration> ports=(ArrayList<HDLVariableDeclaration>)this.ports.clone();
		ports.add(newPorts);
		HDLStatemachineInterface res=new HDLStatemachineInterface(id, container, name, dim, ports, false);
		return res;
	}
	
	/**
	 * Removes a value from the field {@link #getPorts()}.
	 * 
	 * @param newPorts
	 *            the value that should be removed from the field {@link #getPorts()}
	 * @return a new instance of {@link HDLStatemachineInterface} with the updated ports field.
	 */
	@Nonnull
	public HDLStatemachineInterface removePorts(@Nullable HDLVariableDeclaration newPorts){
		if (newPorts == null)
			throw new IllegalArgumentException("Removed element of ports can not be null!");
		ArrayList<HDLVariableDeclaration> ports=(ArrayList<HDLVariableDeclaration>)this.ports.clone();
		ports.remove(newPorts);
		HDLStatemachineInterface res=new HDLStatemachineInterface(id, container, name, dim, ports, false);
		return res;
	}
	
	/**
	 * Removes a value from the field {@link #getPorts()}.
	 * 
	 * @param idx
	 *            the index of the value that should be removed from the field {@link #getPorts()}
	 * @return a new instance of {@link HDLStatemachineInterface} with the updated ports field.
	 */
	@Nonnull
	public HDLStatemachineInterface removePorts(int idx){
		ArrayList<HDLVariableDeclaration> ports=(ArrayList<HDLVariableDeclaration>)this.ports.clone();
		ports.remove(idx);
		HDLStatemachineInterface res=new HDLStatemachineInterface(id, container, name, dim, ports, false);
		return res;
	}	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof AbstractHDLStatemachineInterface))
			return false;
		if (!super.equals(obj))
			return false;
		AbstractHDLStatemachineInterface other = (AbstractHDLStatemachineInterface) obj;
		return true;
	}
	private Integer hashCache;
	
	@Override
	public int hashCode() {
		if (hashCache!=null)
			return hashCache;
		int result = super.hashCode();
		final int prime = 31;
		hashCache=result;
		return result;
	}
	public String toConstructionString(String spacing) {
		boolean first=true;
		StringBuilder sb=new StringBuilder();
		sb.append('\n').append(spacing).append("new HDLStatemachineInterface()");
		if (name!=null){
			sb.append(".setName(").append('"'+name+'"').append(")");
		}
		if (dim!=null){
			if (dim.size()>0){
				sb.append('\n').append(spacing);
				for(HDLExpression o:dim){
					sb.append(".addDim(").append(o.toConstructionString(spacing+"\t\t"));
					sb.append('\n').append(spacing).append(")");
				}
			}
		}
		if (ports!=null){
			if (ports.size()>0){
				sb.append('\n').append(spacing);
				for(HDLVariableDeclaration o:ports){
					sb.append(".addPorts(").append(o.toConstructionString(spacing+"\t\t"));
					sb.append('\n').append(spacing).append(")");
				}
			}
		}
		return sb.toString();
	}
	public void validateAllFields(IHDLObject expectedParent, boolean checkResolve){
		super.validateAllFields(expectedParent, checkResolve);
		}
	public EnumSet<HDLClass> getClassSet(){
		return EnumSet.of(HDLClass.HDLStatemachineInterface,HDLClass.HDLInterface,HDLClass.HDLType,HDLClass.HDLObject);
	}
	public Iterator<IHDLObject> deepIterator() {
	return new Iterator<IHDLObject>() {

	private int pos = 0;
	private Iterator<? extends IHDLObject> current;

		@Override
		public boolean hasNext() {
	if ((current != null) && !current.hasNext())
		current = null;
	while (current == null) {
		switch (pos++) {
					case 0:
						if ((dim != null) && (dim .size() != 0)){
							List<Iterator<? extends IHDLObject>> iters=Lists.newArrayListWithCapacity(dim.size());
							for (HDLExpression o : dim) {
								iters.add(Iterators.forArray(o));
								iters.add(o.deepIterator());
							}
							current=Iterators.concat(iters.iterator());
						}	
						break;
					case 1:
						if ((ports != null) && (ports .size() != 0)){
							List<Iterator<? extends IHDLObject>> iters=Lists.newArrayListWithCapacity(ports.size());
							for (HDLVariableDeclaration o : ports) {
								iters.add(Iterators.forArray(o));
								iters.add(o.deepIterator());
							}
							current=Iterators.concat(iters.iterator());
						}	
						break;
				default:
	return false;
				}
			}
			return (current != null) && current.hasNext();
		}

		@Override
		public IHDLObject next() {
	return current.next();
		}

		@Override
		public void remove() {
	throw new IllegalArgumentException("Not supported");
		}

	};
}
	public Iterator<IHDLObject> iterator() {
	return new Iterator<IHDLObject>() {

	private int pos = 0;
	private Iterator<? extends IHDLObject> current;

		@Override
		public boolean hasNext() {
	if ((current != null) && !current.hasNext())
		current = null;
	while (current == null) {
		switch (pos++) {
					case 0:
						if ((dim != null) && (dim .size() != 0))
							current = dim.iterator();
						break;
					case 1:
						if ((ports != null) && (ports .size() != 0))
							current = ports.iterator();
						break;
				default:
	return false;
				}
			}
			return (current != null) && current.hasNext();
		}

		@Override
		public IHDLObject next() {
	return current.next();
		}

		@Override
		public void remove() {
	throw new IllegalArgumentException("Not supported");
		}

	};
}
}