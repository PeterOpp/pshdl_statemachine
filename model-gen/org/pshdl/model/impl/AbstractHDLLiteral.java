/*******************************************************************************
 * PSHDL is a library and (trans-)compiler for PSHDL input. It generates
 *     output suitable for implementation or simulation of it.
 *     
 *     Copyright (C) 2013 Karsten Becker (feedback (at) pshdl (dot) org)
 * 
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 *     This License does not grant permission to use the trade names, trademarks,
 *     service marks, or product names of the Licensor, except as required for 
 *     reasonable and customary use in describing the origin of the Work.
 * 
 * Contributors:
 *     Karsten Becker - initial API and implementation
 ******************************************************************************/
package org.pshdl.model.impl;

import org.pshdl.model.*;
import org.pshdl.model.utils.*;
import org.pshdl.model.extensions.*;
import org.pshdl.model.validation.*;
import org.pshdl.model.validation.builtin.*;
import org.pshdl.model.validation.Problem.*;
import org.pshdl.model.utils.HDLQuery.HDLFieldAccess;
import org.pshdl.model.impl.*;
import com.google.common.base.*;
import com.google.common.collect.*;
import javax.annotation.*;
import java.util.concurrent.atomic.*;
import java.util.*;

@SuppressWarnings("all")
public abstract class AbstractHDLLiteral extends HDLObject   implements HDLExpression{
	/**
	 * Constructs a new instance of {@link AbstractHDLLiteral}
	 * 
	 * @param container
	 *            the value for container. Can be <code>null</code>.
	 * @param val
	 *            the value for val. Can <b>not</b> be <code>null</code>.
	 * @param str
	 *            the value for str. Can be <code>null</code>.
	 * @param validate
	 *			  if <code>true</code> the parameters will be validated.
	 */
	public AbstractHDLLiteral(int id, @Nullable IHDLObject container, @Nonnull String val, @Nullable Boolean str, boolean validate) {
		super(id, container, validate);
		if (validate)
			val=validateVal(val);
		this.val=val;
		if (validate)
			str=validateStr(str);
		this.str=str;
	}

	public AbstractHDLLiteral() {
		super();
		this.val=null;
		this.str=null;
	}
protected final String val;
	/**
	 * Get the val field. Can <b>not</b> be <code>null</code>.
	 * 
	 * @return the field
	 */
	@Nonnull
	public String getVal(){
		return val;
	}
	protected String validateVal(String val){
		if (val==null)
			throw new IllegalArgumentException("The field val can not be null!");
		return val;
		}
protected final Boolean str;
	/**
	 * Get the str field. Can be <code>null</code>.
	 * 
	 * @return the field
	 */
	@Nullable
	public Boolean getStr(){
		return str;
	}
	protected Boolean validateStr(Boolean str){
		return str;
		}
	/**
	 * Creates a copy of this class with the same fields.
	 * 
	 * @return a new instance of this class.
	 */
	@Nonnull
	public HDLLiteral copy(){
		HDLLiteral newObject=new HDLLiteral(id, null, val, str, false);
		copyMetaData(this, newObject, false);
		return newObject;
	}
	
	/**
	 * Creates a copy of this class with the same fields.
	 * 
	 * @return a new instance of this class.
	 */
	@Nonnull
	public HDLLiteral copyFiltered(CopyFilter filter){
		String filteredval=filter.copyObject("val", this, val);
		Boolean filteredstr=filter.copyObject("str", this, str);
		return filter.postFilter((HDLLiteral)this, new HDLLiteral(id, null, filteredval, filteredstr, false));
	}
	
	/**
	 * Creates a deep copy of this class with the same fields and freezes it.
	 * 
	 * @return a new instance of this class.
	 */
	@Nonnull
	public HDLLiteral copyDeepFrozen(IHDLObject container){
		HDLLiteral copy = copyFiltered(CopyFilter.DEEP_META);
		copy.freeze(container);
		return copy;
	}
	/**
	 * Setter for the field {@link #getContainer()}.
	 * 
	 * @param container
	 *            sets the new container of this object. Can be <code>null</code>.
	 * @return 
	 *			  the same instance of {@link HDLLiteral} with the updated container field.
	 */
	@Nonnull
	public HDLLiteral setContainer(@Nullable IHDLObject container){
		return (HDLLiteral)super.setContainer(container);
	}
	/**
	 * Setter for the field {@link #getVal()}.
	 * 
	 * @param val
	 *            sets the new val of this object. Can <b>not</b> be <code>null</code>.
	 * @return 
	 *			  a new instance of {@link HDLLiteral} with the updated val field.
	 */
	@Nonnull
	public HDLLiteral setVal(@Nonnull String val){
		val=validateVal(val);
		HDLLiteral res=new HDLLiteral(id, container, val, str, false);
		return res;
	}
	/**
	 * Setter for the field {@link #getStr()}.
	 * 
	 * @param str
	 *            sets the new str of this object. Can be <code>null</code>.
	 * @return 
	 *			  a new instance of {@link HDLLiteral} with the updated str field.
	 */
	@Nonnull
	public HDLLiteral setStr(@Nullable Boolean str){
		str=validateStr(str);
		HDLLiteral res=new HDLLiteral(id, container, val, str, false);
		return res;
	}
	/**
	 * Setter for the field {@link #getStr()}.
	 * 
	 * @param str
	 *            sets the new str of this object. Can be <code>null</code>.
	 * @return 
	 *			  a new instance of {@link HDLLiteral} with the updated str field.
	 */
	@Nonnull
	public HDLLiteral setStr(boolean str){
		str=validateStr(str);
		HDLLiteral res=new HDLLiteral(id, container, val, str, false);
		return res;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof AbstractHDLLiteral))
			return false;
		if (!super.equals(obj))
			return false;
		AbstractHDLLiteral other = (AbstractHDLLiteral) obj;
		if (val == null) {
			if (other.val != null)
				return false;
		} else if (!val.equals(other.val))
			return false;
		if (str == null) {
			if (other.str != null)
				return false;
		} else if (!str.equals(other.str))
			return false;
		return true;
	}
	private Integer hashCache;
	
	@Override
	public int hashCode() {
		if (hashCache!=null)
			return hashCache;
		int result = super.hashCode();
		final int prime = 31;
		result = prime * result + ((val == null) ? 0 : val.hashCode());
		result = prime * result + ((str == null) ? 0 : str.hashCode());
		hashCache=result;
		return result;
	}
	public String toConstructionString(String spacing) {
		boolean first=true;
		StringBuilder sb=new StringBuilder();
		sb.append('\n').append(spacing).append("new HDLLiteral()");
		if (val!=null){
			sb.append(".setVal(").append('"'+val+'"').append(")");
		}
		if (str!=null){
			sb.append(".setStr(").append(str).append(")");
		}
		return sb.toString();
	}
	public void validateAllFields(IHDLObject expectedParent, boolean checkResolve){
		super.validateAllFields(expectedParent, checkResolve);
		validateVal(getVal());
		validateStr(getStr());
		}
	public EnumSet<HDLClass> getClassSet(){
		return EnumSet.of(HDLClass.HDLLiteral,HDLClass.HDLExpression,HDLClass.HDLObject);
	}
	public Iterator<IHDLObject> deepIterator() {
	return new Iterator<IHDLObject>() {

	private int pos = 0;
	private Iterator<? extends IHDLObject> current;

		@Override
		public boolean hasNext() {
	if ((current != null) && !current.hasNext())
		current = null;
	while (current == null) {
		switch (pos++) {
				default:
	return false;
				}
			}
			return (current != null) && current.hasNext();
		}

		@Override
		public IHDLObject next() {
	return current.next();
		}

		@Override
		public void remove() {
	throw new IllegalArgumentException("Not supported");
		}

	};
}
	public Iterator<IHDLObject> iterator() {
	return new Iterator<IHDLObject>() {

	private int pos = 0;
	private Iterator<? extends IHDLObject> current;

		@Override
		public boolean hasNext() {
	if ((current != null) && !current.hasNext())
		current = null;
	while (current == null) {
		switch (pos++) {
				default:
	return false;
				}
			}
			return (current != null) && current.hasNext();
		}

		@Override
		public IHDLObject next() {
	return current.next();
		}

		@Override
		public void remove() {
	throw new IllegalArgumentException("Not supported");
		}

	};
}
}