/*******************************************************************************
 * PSHDL is a library and (trans-)compiler for PSHDL input. It generates
 *     output suitable for implementation or simulation of it.
 *     
 *     Copyright (C) 2013 Karsten Becker (feedback (at) pshdl (dot) org)
 * 
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 *     This License does not grant permission to use the trade names, trademarks,
 *     service marks, or product names of the Licensor, except as required for 
 *     reasonable and customary use in describing the origin of the Work.
 * 
 * Contributors:
 *     Karsten Becker - initial API and implementation
 ******************************************************************************/
package org.pshdl.model.impl;

import org.pshdl.model.*;
import org.pshdl.model.utils.*;
import org.pshdl.model.extensions.*;
import org.pshdl.model.validation.*;
import org.pshdl.model.validation.builtin.*;
import org.pshdl.model.validation.Problem.*;
import org.pshdl.model.utils.HDLQuery.HDLFieldAccess;
import org.pshdl.model.impl.*;
import com.google.common.base.*;
import com.google.common.collect.*;
import javax.annotation.*;
import java.util.concurrent.atomic.*;
import java.util.*;

@SuppressWarnings("all")
public abstract class AbstractHDLStatemachineInstantiation extends HDLInstantiation  {
	/**
	 * Constructs a new instance of {@link AbstractHDLStatemachineInstantiation}
	 * 
	 * @param container
	 *            the value for container. Can be <code>null</code>.
	 * @param var
	 *            the value for var. Can <b>not</b> be <code>null</code>.
	 * @param arguments
	 *            the value for arguments. Can be <code>null</code>.
	 * @param hSm
	 *            the value for hSm. Can <b>not</b> be <code>null</code>.
	 * @param validate
	 *			  if <code>true</code> the parameters will be validated.
	 */
	public AbstractHDLStatemachineInstantiation(int id, @Nullable IHDLObject container, @Nonnull HDLVariable var, @Nullable Iterable<HDLArgument> arguments, @Nonnull HDLQualifiedName hSm, boolean validate) {
		super(id, container, var, arguments, validate);
		if (validate)
			hSm=validateHSm(hSm);
		this.hSm=hSm;
	}

	public AbstractHDLStatemachineInstantiation() {
		super();
		this.hSm=null;
	}
protected final HDLQualifiedName hSm;
	@Nullable
	public Optional<HDLStatemachine> resolveHSm(){
		if (!frozen)
			throw new IllegalArgumentException("Object not frozen");
		Optional<HDLStatemachine> res = Optional.absent();
		return res;
	}
	public HDLQualifiedName getHSmRefName(){
		return hSm;
	}
	protected HDLQualifiedName validateHSm(HDLQualifiedName hSm){
		if (hSm==null)
			throw new IllegalArgumentException("The field hSm can not be null!");
		return hSm;
		}
	/**
	 * Creates a copy of this class with the same fields.
	 * 
	 * @return a new instance of this class.
	 */
	@Nonnull
	public HDLStatemachineInstantiation copy(){
		HDLStatemachineInstantiation newObject=new HDLStatemachineInstantiation(id, null, var, arguments, hSm, false);
		copyMetaData(this, newObject, false);
		return newObject;
	}
	
	/**
	 * Creates a copy of this class with the same fields.
	 * 
	 * @return a new instance of this class.
	 */
	@Nonnull
	public HDLStatemachineInstantiation copyFiltered(CopyFilter filter){
		HDLVariable filteredvar=filter.copyObject("var", this, var);
		ArrayList<HDLArgument> filteredarguments=filter.copyContainer("arguments", this, arguments);
		HDLQualifiedName filteredhSm=filter.copyObject("hSm", this, hSm);
		return filter.postFilter((HDLStatemachineInstantiation)this, new HDLStatemachineInstantiation(id, null, filteredvar, filteredarguments, filteredhSm, false));
	}
	
	/**
	 * Creates a deep copy of this class with the same fields and freezes it.
	 * 
	 * @return a new instance of this class.
	 */
	@Nonnull
	public HDLStatemachineInstantiation copyDeepFrozen(IHDLObject container){
		HDLStatemachineInstantiation copy = copyFiltered(CopyFilter.DEEP_META);
		copy.freeze(container);
		return copy;
	}
	/**
	 * Setter for the field {@link #getContainer()}.
	 * 
	 * @param container
	 *            sets the new container of this object. Can be <code>null</code>.
	 * @return 
	 *			  the same instance of {@link HDLStatemachineInstantiation} with the updated container field.
	 */
	@Nonnull
	public HDLStatemachineInstantiation setContainer(@Nullable IHDLObject container){
		return (HDLStatemachineInstantiation)super.setContainer(container);
	}
	/**
	 * Setter for the field {@link #getVar()}.
	 * 
	 * @param var
	 *            sets the new var of this object. Can <b>not</b> be <code>null</code>.
	 * @return 
	 *			  a new instance of {@link HDLStatemachineInstantiation} with the updated var field.
	 */
	@Nonnull
	public HDLStatemachineInstantiation setVar(@Nonnull HDLVariable var){
		var=validateVar(var);
		HDLStatemachineInstantiation res=new HDLStatemachineInstantiation(id, container, var, arguments, hSm, false);
		return res;
	}
	/**
	 * Setter for the field {@link #getArguments()}.
	 * 
	 * @param arguments
	 *            sets the new arguments of this object. Can be <code>null</code>.
	 * @return 
	 *			  a new instance of {@link HDLStatemachineInstantiation} with the updated arguments field.
	 */
	@Nonnull
	public HDLStatemachineInstantiation setArguments(@Nullable Iterable<HDLArgument> arguments){
		arguments=validateArguments(arguments);
		HDLStatemachineInstantiation res=new HDLStatemachineInstantiation(id, container, var, arguments, hSm, false);
		return res;
	}
	
	/**
	 * Adds a new value to the field {@link #getArguments()}.
	 * 
	 * @param newArguments
	 *            the value that should be added to the field {@link #getArguments()}
	 * @return a new instance of {@link HDLStatemachineInstantiation} with the updated arguments field.
	 */
	@Nonnull
	public HDLStatemachineInstantiation addArguments(@Nullable HDLArgument newArguments){
		if (newArguments == null)
			throw new IllegalArgumentException("Element of arguments can not be null!");
		ArrayList<HDLArgument> arguments=(ArrayList<HDLArgument>)this.arguments.clone();
		arguments.add(newArguments);
		HDLStatemachineInstantiation res=new HDLStatemachineInstantiation(id, container, var, arguments, hSm, false);
		return res;
	}
	
	/**
	 * Removes a value from the field {@link #getArguments()}.
	 * 
	 * @param newArguments
	 *            the value that should be removed from the field {@link #getArguments()}
	 * @return a new instance of {@link HDLStatemachineInstantiation} with the updated arguments field.
	 */
	@Nonnull
	public HDLStatemachineInstantiation removeArguments(@Nullable HDLArgument newArguments){
		if (newArguments == null)
			throw new IllegalArgumentException("Removed element of arguments can not be null!");
		ArrayList<HDLArgument> arguments=(ArrayList<HDLArgument>)this.arguments.clone();
		arguments.remove(newArguments);
		HDLStatemachineInstantiation res=new HDLStatemachineInstantiation(id, container, var, arguments, hSm, false);
		return res;
	}
	
	/**
	 * Removes a value from the field {@link #getArguments()}.
	 * 
	 * @param idx
	 *            the index of the value that should be removed from the field {@link #getArguments()}
	 * @return a new instance of {@link HDLStatemachineInstantiation} with the updated arguments field.
	 */
	@Nonnull
	public HDLStatemachineInstantiation removeArguments(int idx){
		ArrayList<HDLArgument> arguments=(ArrayList<HDLArgument>)this.arguments.clone();
		arguments.remove(idx);
		HDLStatemachineInstantiation res=new HDLStatemachineInstantiation(id, container, var, arguments, hSm, false);
		return res;
	}	
	/**
	 * Setter for the field {@link #getHSm()}.
	 * 
	 * @param hSm
	 *            sets the new hSm of this object. Can <b>not</b> be <code>null</code>.
	 * @return 
	 *			  a new instance of {@link HDLStatemachineInstantiation} with the updated hSm field.
	 */
	@Nonnull
	public HDLStatemachineInstantiation setHSm(@Nonnull HDLQualifiedName hSm){
		hSm=validateHSm(hSm);
		HDLStatemachineInstantiation res=new HDLStatemachineInstantiation(id, container, var, arguments, hSm, false);
		return res;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof AbstractHDLStatemachineInstantiation))
			return false;
		if (!super.equals(obj))
			return false;
		AbstractHDLStatemachineInstantiation other = (AbstractHDLStatemachineInstantiation) obj;
		if (hSm == null) {
			if (other.hSm != null)
				return false;
		} else if (!hSm.equals(other.hSm))
			return false;
		return true;
	}
	private Integer hashCache;
	
	@Override
	public int hashCode() {
		if (hashCache!=null)
			return hashCache;
		int result = super.hashCode();
		final int prime = 31;
		result = prime * result + ((hSm == null) ? 0 : hSm.hashCode());
		hashCache=result;
		return result;
	}
	public String toConstructionString(String spacing) {
		boolean first=true;
		StringBuilder sb=new StringBuilder();
		sb.append('\n').append(spacing).append("new HDLStatemachineInstantiation()");
		if (var!=null){
			sb.append(".setVar(").append(var.toConstructionString(spacing+"\t")).append(")");
		}
		if (arguments!=null){
			if (arguments.size()>0){
				sb.append('\n').append(spacing);
				for(HDLArgument o:arguments){
					sb.append(".addArguments(").append(o.toConstructionString(spacing+"\t\t"));
					sb.append('\n').append(spacing).append(")");
				}
			}
		}
		if (hSm!=null){
			sb.append(".setHSm(HDLQualifiedName.create(\"").append(hSm).append("\"))");
		}
		return sb.toString();
	}
	public void validateAllFields(IHDLObject expectedParent, boolean checkResolve){
		super.validateAllFields(expectedParent, checkResolve);
		validateHSm(getHSmRefName());
		if (checkResolve && getHSmRefName()!=null)
		if (!resolveHSm().isPresent())
			throw new HDLProblemException(new Problem(ErrorCode.UNRESOLVED_REFERENCE, this, "to:"+getHSmRefName()));
		}
	public EnumSet<HDLClass> getClassSet(){
		return EnumSet.of(HDLClass.HDLStatemachineInstantiation,HDLClass.HDLInstantiation,HDLClass.HDLStatement,HDLClass.HDLObject);
	}
	public Iterator<IHDLObject> deepIterator() {
	return new Iterator<IHDLObject>() {

	private int pos = 0;
	private Iterator<? extends IHDLObject> current;

		@Override
		public boolean hasNext() {
	if ((current != null) && !current.hasNext())
		current = null;
	while (current == null) {
		switch (pos++) {
					case 0:
						if (var!=null)
							current=Iterators.concat(Iterators.forArray(var), var.deepIterator());
						break;
					case 1:
						if ((arguments != null) && (arguments .size() != 0)){
							List<Iterator<? extends IHDLObject>> iters=Lists.newArrayListWithCapacity(arguments.size());
							for (HDLArgument o : arguments) {
								iters.add(Iterators.forArray(o));
								iters.add(o.deepIterator());
							}
							current=Iterators.concat(iters.iterator());
						}	
						break;
				default:
	return false;
				}
			}
			return (current != null) && current.hasNext();
		}

		@Override
		public IHDLObject next() {
	return current.next();
		}

		@Override
		public void remove() {
	throw new IllegalArgumentException("Not supported");
		}

	};
}
	public Iterator<IHDLObject> iterator() {
	return new Iterator<IHDLObject>() {

	private int pos = 0;
	private Iterator<? extends IHDLObject> current;

		@Override
		public boolean hasNext() {
	if ((current != null) && !current.hasNext())
		current = null;
	while (current == null) {
		switch (pos++) {
					case 0:
						if (var!=null)
							current=Iterators.singletonIterator(var);
						break;
					case 1:
						if ((arguments != null) && (arguments .size() != 0))
							current = arguments.iterator();
						break;
				default:
	return false;
				}
			}
			return (current != null) && current.hasNext();
		}

		@Override
		public IHDLObject next() {
	return current.next();
		}

		@Override
		public void remove() {
	throw new IllegalArgumentException("Not supported");
		}

	};
}
}