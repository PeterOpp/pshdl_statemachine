/*******************************************************************************
 * PSHDL is a library and (trans-)compiler for PSHDL input. It generates
 *     output suitable for implementation or simulation of it.
 *     
 *     Copyright (C) 2013 Karsten Becker (feedback (at) pshdl (dot) org)
 * 
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 *     This License does not grant permission to use the trade names, trademarks,
 *     service marks, or product names of the Licensor, except as required for 
 *     reasonable and customary use in describing the origin of the Work.
 * 
 * Contributors:
 *     Karsten Becker - initial API and implementation
 ******************************************************************************/
package org.pshdl.model.impl;

import org.pshdl.model.*;
import org.pshdl.model.utils.*;
import org.pshdl.model.extensions.*;
import org.pshdl.model.validation.*;
import org.pshdl.model.validation.builtin.*;
import org.pshdl.model.validation.Problem.*;
import org.pshdl.model.utils.HDLQuery.HDLFieldAccess;
import org.pshdl.model.impl.*;
import com.google.common.base.*;
import com.google.common.collect.*;
import javax.annotation.*;
import java.util.concurrent.atomic.*;
import java.util.*;

@SuppressWarnings("all")
public abstract class AbstractHDLInlineStatemachine extends HDLObject   implements HDLStatement{
	/**
	 * Constructs a new instance of {@link AbstractHDLInlineStatemachine}
	 * 
	 * @param container
	 *            the value for container. Can be <code>null</code>.
	 * @param statemachine
	 *            the value for statemachine. Can <b>not</b> be <code>null</code>.
	 * @param validate
	 *			  if <code>true</code> the parameters will be validated.
	 */
	public AbstractHDLInlineStatemachine(int id, @Nullable IHDLObject container, @Nonnull HDLStatemachine statemachine, boolean validate) {
		super(id, container, validate);
		if (validate)
			statemachine=validateStatemachine(statemachine);
		if (statemachine!=null)
			this.statemachine=(HDLStatemachine)statemachine;
		else
			this.statemachine=null;
	}

	public AbstractHDLInlineStatemachine() {
		super();
		this.statemachine=null;
	}
protected final HDLStatemachine statemachine;
	/**
	 * Get the statemachine field. Can <b>not</b> be <code>null</code>.
	 * 
	 * @return the field
	 */
	@Nonnull
	public HDLStatemachine getStatemachine(){
		return statemachine;
	}
	protected HDLStatemachine validateStatemachine(HDLStatemachine statemachine){
		if (statemachine==null)
			throw new IllegalArgumentException("The field statemachine can not be null!");
		return statemachine;
		}
	/**
	 * Creates a copy of this class with the same fields.
	 * 
	 * @return a new instance of this class.
	 */
	@Nonnull
	public HDLInlineStatemachine copy(){
		HDLInlineStatemachine newObject=new HDLInlineStatemachine(id, null, statemachine, false);
		copyMetaData(this, newObject, false);
		return newObject;
	}
	
	/**
	 * Creates a copy of this class with the same fields.
	 * 
	 * @return a new instance of this class.
	 */
	@Nonnull
	public HDLInlineStatemachine copyFiltered(CopyFilter filter){
		HDLStatemachine filteredstatemachine=filter.copyObject("statemachine", this, statemachine);
		return filter.postFilter((HDLInlineStatemachine)this, new HDLInlineStatemachine(id, null, filteredstatemachine, false));
	}
	
	/**
	 * Creates a deep copy of this class with the same fields and freezes it.
	 * 
	 * @return a new instance of this class.
	 */
	@Nonnull
	public HDLInlineStatemachine copyDeepFrozen(IHDLObject container){
		HDLInlineStatemachine copy = copyFiltered(CopyFilter.DEEP_META);
		copy.freeze(container);
		return copy;
	}
	/**
	 * Setter for the field {@link #getContainer()}.
	 * 
	 * @param container
	 *            sets the new container of this object. Can be <code>null</code>.
	 * @return 
	 *			  the same instance of {@link HDLInlineStatemachine} with the updated container field.
	 */
	@Nonnull
	public HDLInlineStatemachine setContainer(@Nullable IHDLObject container){
		return (HDLInlineStatemachine)super.setContainer(container);
	}
	/**
	 * Setter for the field {@link #getStatemachine()}.
	 * 
	 * @param statemachine
	 *            sets the new statemachine of this object. Can <b>not</b> be <code>null</code>.
	 * @return 
	 *			  a new instance of {@link HDLInlineStatemachine} with the updated statemachine field.
	 */
	@Nonnull
	public HDLInlineStatemachine setStatemachine(@Nonnull HDLStatemachine statemachine){
		statemachine=validateStatemachine(statemachine);
		HDLInlineStatemachine res=new HDLInlineStatemachine(id, container, statemachine, false);
		return res;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof AbstractHDLInlineStatemachine))
			return false;
		if (!super.equals(obj))
			return false;
		AbstractHDLInlineStatemachine other = (AbstractHDLInlineStatemachine) obj;
		if (statemachine == null) {
			if (other.statemachine != null)
				return false;
		} else if (!statemachine.equals(other.statemachine))
			return false;
		return true;
	}
	private Integer hashCache;
	
	@Override
	public int hashCode() {
		if (hashCache!=null)
			return hashCache;
		int result = super.hashCode();
		final int prime = 31;
		result = prime * result + ((statemachine == null) ? 0 : statemachine.hashCode());
		hashCache=result;
		return result;
	}
	public String toConstructionString(String spacing) {
		boolean first=true;
		StringBuilder sb=new StringBuilder();
		sb.append('\n').append(spacing).append("new HDLInlineStatemachine()");
		if (statemachine!=null){
			sb.append(".setStatemachine(").append(statemachine.toConstructionString(spacing+"\t")).append(")");
		}
		return sb.toString();
	}
	public void validateAllFields(IHDLObject expectedParent, boolean checkResolve){
		super.validateAllFields(expectedParent, checkResolve);
		validateStatemachine(getStatemachine());
		if (getStatemachine()!=null){
			getStatemachine().validateAllFields(this, checkResolve);
		}
		}
	public EnumSet<HDLClass> getClassSet(){
		return EnumSet.of(HDLClass.HDLInlineStatemachine,HDLClass.HDLStatement,HDLClass.HDLObject);
	}
	public Iterator<IHDLObject> deepIterator() {
	return new Iterator<IHDLObject>() {

	private int pos = 0;
	private Iterator<? extends IHDLObject> current;

		@Override
		public boolean hasNext() {
	if ((current != null) && !current.hasNext())
		current = null;
	while (current == null) {
		switch (pos++) {
					case 0:
						if (statemachine!=null)
							current=Iterators.concat(Iterators.forArray(statemachine), statemachine.deepIterator());
						break;
				default:
	return false;
				}
			}
			return (current != null) && current.hasNext();
		}

		@Override
		public IHDLObject next() {
	return current.next();
		}

		@Override
		public void remove() {
	throw new IllegalArgumentException("Not supported");
		}

	};
}
	public Iterator<IHDLObject> iterator() {
	return new Iterator<IHDLObject>() {

	private int pos = 0;
	private Iterator<? extends IHDLObject> current;

		@Override
		public boolean hasNext() {
	if ((current != null) && !current.hasNext())
		current = null;
	while (current == null) {
		switch (pos++) {
					case 0:
						if (statemachine!=null)
							current=Iterators.singletonIterator(statemachine);
						break;
				default:
	return false;
				}
			}
			return (current != null) && current.hasNext();
		}

		@Override
		public IHDLObject next() {
	return current.next();
		}

		@Override
		public void remove() {
	throw new IllegalArgumentException("Not supported");
		}

	};
}
}