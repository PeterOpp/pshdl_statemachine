/*******************************************************************************
 * PSHDL is a library and (trans-)compiler for PSHDL input. It generates
 *     output suitable for implementation or simulation of it.
 *     
 *     Copyright (C) 2013 Karsten Becker (feedback (at) pshdl (dot) org)
 * 
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 *     This License does not grant permission to use the trade names, trademarks,
 *     service marks, or product names of the Licensor, except as required for 
 *     reasonable and customary use in describing the origin of the Work.
 * 
 * Contributors:
 *     Karsten Becker - initial API and implementation
 ******************************************************************************/
package org.pshdl.model.impl;

import org.pshdl.model.*;
import org.pshdl.model.utils.*;
import org.pshdl.model.extensions.*;
import org.pshdl.model.validation.*;
import org.pshdl.model.validation.builtin.*;
import org.pshdl.model.validation.Problem.*;
import org.pshdl.model.utils.HDLQuery.HDLFieldAccess;
import org.pshdl.model.impl.*;
import com.google.common.base.*;
import com.google.common.collect.*;
import javax.annotation.*;
import java.util.concurrent.atomic.*;
import java.util.*;

@SuppressWarnings("all")
public abstract class AbstractHDLStatemachineReturnStatement extends HDLObject   implements HDLStatement{
	/**
	 * Constructs a new instance of {@link AbstractHDLStatemachineReturnStatement}
	 * 
	 * @param container
	 *            the value for container. Can be <code>null</code>.
	 * @param returnVar
	 *            the value for returnVar. Can be <code>null</code>.
	 * @param validate
	 *			  if <code>true</code> the parameters will be validated.
	 */
	public AbstractHDLStatemachineReturnStatement(int id, @Nullable IHDLObject container, @Nullable HDLExpression returnVar, boolean validate) {
		super(id, container, validate);
		if (validate)
			returnVar=validateReturnVar(returnVar);
		if (returnVar!=null)
			this.returnVar=(HDLExpression)returnVar;
		else
			this.returnVar=null;
	}

	public AbstractHDLStatemachineReturnStatement() {
		super();
		this.returnVar=null;
	}
protected final HDLExpression returnVar;
	/**
	 * Get the returnVar field. Can be <code>null</code>.
	 * 
	 * @return the field
	 */
	@Nullable
	public HDLExpression getReturnVar(){
		return returnVar;
	}
	protected HDLExpression validateReturnVar(HDLExpression returnVar){
		return returnVar;
		}
	/**
	 * Creates a copy of this class with the same fields.
	 * 
	 * @return a new instance of this class.
	 */
	@Nonnull
	public HDLStatemachineReturnStatement copy(){
		HDLStatemachineReturnStatement newObject=new HDLStatemachineReturnStatement(id, null, returnVar, false);
		copyMetaData(this, newObject, false);
		return newObject;
	}
	
	/**
	 * Creates a copy of this class with the same fields.
	 * 
	 * @return a new instance of this class.
	 */
	@Nonnull
	public HDLStatemachineReturnStatement copyFiltered(CopyFilter filter){
		HDLExpression filteredreturnVar=filter.copyObject("returnVar", this, returnVar);
		return filter.postFilter((HDLStatemachineReturnStatement)this, new HDLStatemachineReturnStatement(id, null, filteredreturnVar, false));
	}
	
	/**
	 * Creates a deep copy of this class with the same fields and freezes it.
	 * 
	 * @return a new instance of this class.
	 */
	@Nonnull
	public HDLStatemachineReturnStatement copyDeepFrozen(IHDLObject container){
		HDLStatemachineReturnStatement copy = copyFiltered(CopyFilter.DEEP_META);
		copy.freeze(container);
		return copy;
	}
	/**
	 * Setter for the field {@link #getContainer()}.
	 * 
	 * @param container
	 *            sets the new container of this object. Can be <code>null</code>.
	 * @return 
	 *			  the same instance of {@link HDLStatemachineReturnStatement} with the updated container field.
	 */
	@Nonnull
	public HDLStatemachineReturnStatement setContainer(@Nullable IHDLObject container){
		return (HDLStatemachineReturnStatement)super.setContainer(container);
	}
	/**
	 * Setter for the field {@link #getReturnVar()}.
	 * 
	 * @param returnVar
	 *            sets the new returnVar of this object. Can be <code>null</code>.
	 * @return 
	 *			  a new instance of {@link HDLStatemachineReturnStatement} with the updated returnVar field.
	 */
	@Nonnull
	public HDLStatemachineReturnStatement setReturnVar(@Nullable HDLExpression returnVar){
		returnVar=validateReturnVar(returnVar);
		HDLStatemachineReturnStatement res=new HDLStatemachineReturnStatement(id, container, returnVar, false);
		return res;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof AbstractHDLStatemachineReturnStatement))
			return false;
		if (!super.equals(obj))
			return false;
		AbstractHDLStatemachineReturnStatement other = (AbstractHDLStatemachineReturnStatement) obj;
		if (returnVar == null) {
			if (other.returnVar != null)
				return false;
		} else if (!returnVar.equals(other.returnVar))
			return false;
		return true;
	}
	private Integer hashCache;
	
	@Override
	public int hashCode() {
		if (hashCache!=null)
			return hashCache;
		int result = super.hashCode();
		final int prime = 31;
		result = prime * result + ((returnVar == null) ? 0 : returnVar.hashCode());
		hashCache=result;
		return result;
	}
	public String toConstructionString(String spacing) {
		boolean first=true;
		StringBuilder sb=new StringBuilder();
		sb.append('\n').append(spacing).append("new HDLStatemachineReturnStatement()");
		if (returnVar!=null){
			sb.append(".setReturnVar(").append(returnVar.toConstructionString(spacing+"\t")).append(")");
		}
		return sb.toString();
	}
	public void validateAllFields(IHDLObject expectedParent, boolean checkResolve){
		super.validateAllFields(expectedParent, checkResolve);
		validateReturnVar(getReturnVar());
		if (getReturnVar()!=null){
			getReturnVar().validateAllFields(this, checkResolve);
		}
		}
	public EnumSet<HDLClass> getClassSet(){
		return EnumSet.of(HDLClass.HDLStatemachineReturnStatement,HDLClass.HDLStatement,HDLClass.HDLObject);
	}
	public Iterator<IHDLObject> deepIterator() {
	return new Iterator<IHDLObject>() {

	private int pos = 0;
	private Iterator<? extends IHDLObject> current;

		@Override
		public boolean hasNext() {
	if ((current != null) && !current.hasNext())
		current = null;
	while (current == null) {
		switch (pos++) {
					case 0:
						if (returnVar!=null)
							current=Iterators.concat(Iterators.forArray(returnVar), returnVar.deepIterator());
						break;
				default:
	return false;
				}
			}
			return (current != null) && current.hasNext();
		}

		@Override
		public IHDLObject next() {
	return current.next();
		}

		@Override
		public void remove() {
	throw new IllegalArgumentException("Not supported");
		}

	};
}
	public Iterator<IHDLObject> iterator() {
	return new Iterator<IHDLObject>() {

	private int pos = 0;
	private Iterator<? extends IHDLObject> current;

		@Override
		public boolean hasNext() {
	if ((current != null) && !current.hasNext())
		current = null;
	while (current == null) {
		switch (pos++) {
					case 0:
						if (returnVar!=null)
							current=Iterators.singletonIterator(returnVar);
						break;
				default:
	return false;
				}
			}
			return (current != null) && current.hasNext();
		}

		@Override
		public IHDLObject next() {
	return current.next();
		}

		@Override
		public void remove() {
	throw new IllegalArgumentException("Not supported");
		}

	};
}
}