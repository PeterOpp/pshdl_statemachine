library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.Casts.ALL;
use work.ShiftOps.ALL;
use work.Types.ALL;
entity counter is
    port (
        clk : in std_logic;
        rst : in std_logic;
        clk_cnt : in std_logic_vector(15 downto 0);
        zero : out std_logic
    );
end;
architecture pshdlGenerated of counter is
    signal count : unsigned(15 downto 0);
begin
    process(count, rst)
    begin
        zero <= '0';
        if ((count = TO_UNSIGNED(0, 16)) and (rst = '0')) then
            zero <= '1';
        end if;
    end process;
    process(clk)
    begin
        if RISING_EDGE(clk) then
            if rst = '1' then
                count <= (others => '0');
            else
                if ((rst = '1') or (count = TO_UNSIGNED(0, 16))) then
                    count <= resizeNatural(bitvectorToNatural(clk_cnt), 16);
                else
                    count <= (count - 1);
                end if;
            end if;
        end if;
    end process;
end;
