library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.Casts.ALL;
use work.ShiftOps.ALL;
use work.Types.ALL;
use work.cmdTypePkg.all;
entity I2C_GetPoti is
    port (
        clk : in std_logic;
        rst : in std_logic;
        sda : in std_logic;
        scl : in std_logic;
        sda_oen : out std_logic;
        scl_oen : out std_logic;
        \$smStart\ : in std_logic;
        \$smDone\ : out std_logic;
        \$smReturn\ : out std_logic_vector(7 downto 0)
    );
    type States is (Idle, State_cmd_ctrl_run_1, State_cmd_ctrl_wait_2, State_byteWriter_run_3, State_byteWriter_wait_4, State_bitReader_run_5, State_bitReader_wait_6, State_7, State_byteWriter_run_8, State_byteWriter_wait_9, State_bitReader_run_10, State_bitReader_wait_11, State_12, State_cmd_ctrl_run_13, State_cmd_ctrl_wait_14, State_byteWriter_run_15, State_byteWriter_wait_16, State_bitReader_run_17, State_bitReader_wait_18, State_19, State_byteReader_run_20, State_byteReader_wait_21, State_cmd_ctrl_run_22, State_cmd_ctrl_wait_23, State_24);
end;
architecture pshdlGenerated of I2C_GetPoti is
    signal \$State\ : States;
    signal Byte : std_logic_vector(7 downto 0);
    signal gotAck : std_logic;
    signal \$map_cnt_clk_cnt\ : std_logic_vector(15 downto 0);
    signal \$map_cnt_zero\ : std_logic;
    signal \$map_cnt_clk\ : std_logic;
    signal \$map_cnt_rst\ : std_logic;
    signal \$map_cmd_ctrl_scl_oen\ : std_logic;
    signal \$map_cmd_ctrl_scl_oen_prev\ : std_logic;
    signal \$map_cmd_ctrl_sda_oen\ : std_logic;
    signal \$map_cmd_ctrl_swapState\ : std_logic;
    signal \$map_cmd_ctrl_cType\ : cmdType;
    signal \$map_cmd_ctrl_$smStart\ : std_logic;
    signal \$map_cmd_ctrl_$smDone\ : std_logic;
    signal \$map_cmd_ctrl_$smReturn\ : std_logic;
    signal \$map_cmd_ctrl_clk\ : std_logic;
    signal \$map_cmd_ctrl_rst\ : std_logic;
    signal \$map_byteWriter_byte\ : std_logic_vector(7 downto 0);
    signal \$map_byteWriter_scl_oen\ : std_logic;
    signal \$map_byteWriter_sda_oen\ : std_logic;
    signal \$map_byteWriter_swapState\ : std_logic;
    signal \$map_byteWriter_$smStart\ : std_logic;
    signal \$map_byteWriter_$smDone\ : std_logic;
    signal \$map_byteWriter_$smReturn\ : std_logic;
    signal \$map_byteWriter_clk\ : std_logic;
    signal \$map_byteWriter_rst\ : std_logic;
    signal \$map_byteReader_bitReader_sda_oen\ : std_logic;
    signal \$map_byteReader_bitReader_scl_oen\ : std_logic;
    signal \$map_byteReader_bitReader_sda\ : std_logic;
    signal \$map_byteReader_bitReader_swapState\ : std_logic;
    signal \$map_byteReader_bitReader_$smStart\ : std_logic;
    signal \$map_byteReader_bitReader_$smDone\ : std_logic;
    signal \$map_byteReader_bitReader_$smReturn\ : std_logic;
    signal \$map_byteReader_bitReader_clk\ : std_logic;
    signal \$map_byteReader_bitReader_rst\ : std_logic;
    signal \$map_byteReader_scl_oen\ : std_logic;
    signal \$map_byteReader_sda_oen\ : std_logic;
    signal \$map_byteReader_sda\ : std_logic;
    signal \$map_byteReader_swapState\ : std_logic;
    signal \$map_byteReader_$smStart\ : std_logic;
    signal \$map_byteReader_$smDone\ : std_logic;
    signal \$map_byteReader_$smReturn\ : std_logic_vector(7 downto 0);
    signal \$map_byteReader_clk\ : std_logic;
    signal \$map_byteReader_rst\ : std_logic;
    signal \$map_bitReader_sda_oen\ : std_logic;
    signal \$map_bitReader_scl_oen\ : std_logic;
    signal \$map_bitReader_sda\ : std_logic;
    signal \$map_bitReader_swapState\ : std_logic;
    signal \$map_bitReader_$smStart\ : std_logic;
    signal \$map_bitReader_$smDone\ : std_logic;
    signal \$map_bitReader_$smReturn\ : std_logic;
    signal \$map_bitReader_clk\ : std_logic;
    signal \$map_bitReader_rst\ : std_logic;
begin
    cnt : entity work.counter
        port map (
            clk_cnt => \$map_cnt_clk_cnt\,
            zero => \$map_cnt_zero\,
            clk => \$map_cnt_clk\,
            rst => \$map_cnt_rst\
        );
    cmd_ctrl : entity work.send_cmd
        port map (
            scl_oen => \$map_cmd_ctrl_scl_oen\,
            scl_oen_prev => \$map_cmd_ctrl_scl_oen_prev\,
            sda_oen => \$map_cmd_ctrl_sda_oen\,
            swapState => \$map_cmd_ctrl_swapState\,
            cType => \$map_cmd_ctrl_cType\,
            \$smStart\ => \$map_cmd_ctrl_$smStart\,
            \$smDone\ => \$map_cmd_ctrl_$smDone\,
            \$smReturn\ => \$map_cmd_ctrl_$smReturn\,
            clk => \$map_cmd_ctrl_clk\,
            rst => \$map_cmd_ctrl_rst\
        );
    byteWriter : entity work.writeByte
        port map (
            byte => \$map_byteWriter_byte\,
            scl_oen => \$map_byteWriter_scl_oen\,
            sda_oen => \$map_byteWriter_sda_oen\,
            swapState => \$map_byteWriter_swapState\,
            \$smStart\ => \$map_byteWriter_$smStart\,
            \$smDone\ => \$map_byteWriter_$smDone\,
            \$smReturn\ => \$map_byteWriter_$smReturn\,
            clk => \$map_byteWriter_clk\,
            rst => \$map_byteWriter_rst\
        );
    byteReader : entity work.readByte
        port map (
            bitReader_sda_oen => \$map_byteReader_bitReader_sda_oen\,
            bitReader_scl_oen => \$map_byteReader_bitReader_scl_oen\,
            bitReader_sda => \$map_byteReader_bitReader_sda\,
            bitReader_swapState => \$map_byteReader_bitReader_swapState\,
            \bitReader_$smStart\ => \$map_byteReader_bitReader_$smStart\,
            \bitReader_$smDone\ => \$map_byteReader_bitReader_$smDone\,
            \bitReader_$smReturn\ => \$map_byteReader_bitReader_$smReturn\,
            bitReader_clk => \$map_byteReader_bitReader_clk\,
            bitReader_rst => \$map_byteReader_bitReader_rst\,
            scl_oen => \$map_byteReader_scl_oen\,
            sda_oen => \$map_byteReader_sda_oen\,
            sda => \$map_byteReader_sda\,
            swapState => \$map_byteReader_swapState\,
            \$smStart\ => \$map_byteReader_$smStart\,
            \$smDone\ => \$map_byteReader_$smDone\,
            \$smReturn\ => \$map_byteReader_$smReturn\,
            clk => \$map_byteReader_clk\,
            rst => \$map_byteReader_rst\
        );
    bitReader : entity work.readBit
        port map (
            sda_oen => \$map_bitReader_sda_oen\,
            scl_oen => \$map_bitReader_scl_oen\,
            sda => \$map_bitReader_sda\,
            swapState => \$map_bitReader_swapState\,
            \$smStart\ => \$map_bitReader_$smStart\,
            \$smDone\ => \$map_bitReader_$smDone\,
            \$smReturn\ => \$map_bitReader_$smReturn\,
            clk => \$map_bitReader_clk\,
            rst => \$map_bitReader_rst\
        );
    process(\$State\, \$map_bitReader_$smDone\, \$map_bitReader_$smReturn\, \$map_bitReader_scl_oen\, \$map_bitReader_sda_oen\, \$map_byteReader_$smDone\, \$map_byteReader_bitReader_$smStart\, \$map_byteReader_bitReader_clk\, \$map_byteReader_bitReader_rst\, \$map_byteReader_bitReader_sda\, \$map_byteReader_bitReader_swapState\, \$map_byteReader_scl_oen\, \$map_byteReader_sda_oen\, \$map_byteWriter_$smDone\, \$map_byteWriter_scl_oen\, \$map_byteWriter_sda_oen\, \$map_cmd_ctrl_$smDone\, \$map_cmd_ctrl_scl_oen\, \$map_cmd_ctrl_sda_oen\, \$map_cnt_zero\, \$smStart\, clk, gotAck, rst, scl, sda)
    begin
        \$smDone\ <= '0';
        \$map_cmd_ctrl_scl_oen_prev\ <= '0';
        \$map_cmd_ctrl_swapState\ <= '0';
        \$map_cmd_ctrl_cType\ <= START;
        \$map_byteWriter_byte\ <= (others => '0');
        \$map_byteWriter_swapState\ <= '0';
        \$map_byteReader_bitReader_sda_oen\ <= '0';
        \$map_byteReader_bitReader_scl_oen\ <= '0';
        \$map_byteReader_bitReader_$smDone\ <= '0';
        \$map_byteReader_bitReader_$smReturn\ <= '0';
        \$map_byteReader_sda\ <= '0';
        \$map_byteReader_swapState\ <= '0';
        \$map_bitReader_sda\ <= '0';
        \$map_bitReader_swapState\ <= '0';
        sda_oen <= '0';
        scl_oen <= '0';
        \$map_cnt_clk_cnt\ <= b"0000000010100101";
        \$map_cnt_clk\ <= clk;
        \$map_cnt_rst\ <= rst;
        \$map_cmd_ctrl_clk\ <= clk;
        \$map_cmd_ctrl_rst\ <= rst;
        \$map_cmd_ctrl_$smStart\ <= '0';
        \$map_byteWriter_clk\ <= clk;
        \$map_byteWriter_rst\ <= rst;
        \$map_byteWriter_$smStart\ <= '0';
        \$map_byteReader_clk\ <= clk;
        \$map_byteReader_rst\ <= rst;
        \$map_byteReader_$smStart\ <= '0';
        \$map_bitReader_clk\ <= clk;
        \$map_bitReader_rst\ <= rst;
        \$map_bitReader_$smStart\ <= '0';
        case \$State\ is
            when Idle =>
                sda_oen <= '1';
                scl_oen <= '1';
                \$smDone\ <= '1';

            when State_cmd_ctrl_run_1 =>
                scl_oen <= \$map_cmd_ctrl_scl_oen\;
                \$map_cmd_ctrl_scl_oen_prev\ <= scl;
                sda_oen <= \$map_cmd_ctrl_sda_oen\;
                \$map_cmd_ctrl_swapState\ <= \$map_cnt_zero\;
                \$map_cmd_ctrl_cType\ <= START;
                \$map_cmd_ctrl_$smStart\ <= '1';

            when State_cmd_ctrl_wait_2 =>
                scl_oen <= \$map_cmd_ctrl_scl_oen\;
                \$map_cmd_ctrl_scl_oen_prev\ <= scl;
                sda_oen <= \$map_cmd_ctrl_sda_oen\;
                \$map_cmd_ctrl_swapState\ <= \$map_cnt_zero\;
                \$map_cmd_ctrl_cType\ <= START;

            when State_byteWriter_run_3 =>
                \$map_byteWriter_byte\ <= b"10010000";
                scl_oen <= \$map_byteWriter_scl_oen\;
                sda_oen <= \$map_byteWriter_sda_oen\;
                \$map_byteWriter_swapState\ <= \$map_cnt_zero\;
                \$map_byteWriter_$smStart\ <= '1';

            when State_byteWriter_wait_4 =>
                \$map_byteWriter_byte\ <= b"10010000";
                scl_oen <= \$map_byteWriter_scl_oen\;
                sda_oen <= \$map_byteWriter_sda_oen\;
                \$map_byteWriter_swapState\ <= \$map_cnt_zero\;

            when State_bitReader_run_5 =>
                sda_oen <= \$map_bitReader_sda_oen\;
                scl_oen <= \$map_bitReader_scl_oen\;
                \$map_bitReader_sda\ <= sda;
                \$map_bitReader_swapState\ <= \$map_cnt_zero\;
                \$map_bitReader_$smStart\ <= '1';

            when State_bitReader_wait_6 =>
                sda_oen <= \$map_bitReader_sda_oen\;
                scl_oen <= \$map_bitReader_scl_oen\;
                \$map_bitReader_sda\ <= sda;
                \$map_bitReader_swapState\ <= \$map_cnt_zero\;

            when State_7 =>

            when State_byteWriter_run_8 =>
                \$map_byteWriter_byte\ <= b"10001100";
                scl_oen <= \$map_byteWriter_scl_oen\;
                sda_oen <= \$map_byteWriter_sda_oen\;
                \$map_byteWriter_swapState\ <= \$map_cnt_zero\;
                \$map_byteWriter_$smStart\ <= '1';

            when State_byteWriter_wait_9 =>
                \$map_byteWriter_byte\ <= b"10001100";
                scl_oen <= \$map_byteWriter_scl_oen\;
                sda_oen <= \$map_byteWriter_sda_oen\;
                \$map_byteWriter_swapState\ <= \$map_cnt_zero\;

            when State_bitReader_run_10 =>
                sda_oen <= \$map_bitReader_sda_oen\;
                scl_oen <= \$map_bitReader_scl_oen\;
                \$map_bitReader_sda\ <= sda;
                \$map_bitReader_swapState\ <= \$map_cnt_zero\;
                \$map_bitReader_$smStart\ <= '1';

            when State_bitReader_wait_11 =>
                sda_oen <= \$map_bitReader_sda_oen\;
                scl_oen <= \$map_bitReader_scl_oen\;
                \$map_bitReader_sda\ <= sda;
                \$map_bitReader_swapState\ <= \$map_cnt_zero\;

            when State_12 =>

            when State_cmd_ctrl_run_13 =>
                scl_oen <= \$map_cmd_ctrl_scl_oen\;
                \$map_cmd_ctrl_scl_oen_prev\ <= scl;
                sda_oen <= \$map_cmd_ctrl_sda_oen\;
                \$map_cmd_ctrl_swapState\ <= \$map_cnt_zero\;
                \$map_cmd_ctrl_cType\ <= START;
                \$map_cmd_ctrl_$smStart\ <= '1';

            when State_cmd_ctrl_wait_14 =>
                scl_oen <= \$map_cmd_ctrl_scl_oen\;
                \$map_cmd_ctrl_scl_oen_prev\ <= scl;
                sda_oen <= \$map_cmd_ctrl_sda_oen\;
                \$map_cmd_ctrl_swapState\ <= \$map_cnt_zero\;
                \$map_cmd_ctrl_cType\ <= START;

            when State_byteWriter_run_15 =>
                \$map_byteWriter_byte\ <= b"10010001";
                scl_oen <= \$map_byteWriter_scl_oen\;
                sda_oen <= \$map_byteWriter_sda_oen\;
                \$map_byteWriter_swapState\ <= \$map_cnt_zero\;
                \$map_byteWriter_$smStart\ <= '1';

            when State_byteWriter_wait_16 =>
                \$map_byteWriter_byte\ <= b"10010001";
                scl_oen <= \$map_byteWriter_scl_oen\;
                sda_oen <= \$map_byteWriter_sda_oen\;
                \$map_byteWriter_swapState\ <= \$map_cnt_zero\;

            when State_bitReader_run_17 =>
                sda_oen <= \$map_bitReader_sda_oen\;
                scl_oen <= \$map_bitReader_scl_oen\;
                \$map_bitReader_sda\ <= sda;
                \$map_bitReader_swapState\ <= \$map_cnt_zero\;
                \$map_bitReader_$smStart\ <= '1';

            when State_bitReader_wait_18 =>
                sda_oen <= \$map_bitReader_sda_oen\;
                scl_oen <= \$map_bitReader_scl_oen\;
                \$map_bitReader_sda\ <= sda;
                \$map_bitReader_swapState\ <= \$map_cnt_zero\;

            when State_19 =>

            when State_byteReader_run_20 =>
                \$map_byteReader_bitReader_sda_oen\ <= \$map_bitReader_sda_oen\;
                \$map_byteReader_bitReader_scl_oen\ <= \$map_bitReader_scl_oen\;
                \$map_bitReader_sda\ <= \$map_byteReader_bitReader_sda\;
                \$map_bitReader_swapState\ <= \$map_byteReader_bitReader_swapState\;
                \$map_bitReader_$smStart\ <= \$map_byteReader_bitReader_$smStart\;
                \$map_byteReader_bitReader_$smDone\ <= \$map_bitReader_$smDone\;
                \$map_byteReader_bitReader_$smReturn\ <= \$map_bitReader_$smReturn\;
                \$map_bitReader_clk\ <= \$map_byteReader_bitReader_clk\;
                \$map_bitReader_rst\ <= \$map_byteReader_bitReader_rst\;
                scl_oen <= \$map_byteReader_scl_oen\;
                sda_oen <= \$map_byteReader_sda_oen\;
                \$map_byteReader_sda\ <= sda;
                \$map_byteReader_swapState\ <= \$map_cnt_zero\;
                \$map_byteReader_$smStart\ <= '1';

            when State_byteReader_wait_21 =>
                \$map_byteReader_bitReader_sda_oen\ <= \$map_bitReader_sda_oen\;
                \$map_byteReader_bitReader_scl_oen\ <= \$map_bitReader_scl_oen\;
                \$map_bitReader_sda\ <= \$map_byteReader_bitReader_sda\;
                \$map_bitReader_swapState\ <= \$map_byteReader_bitReader_swapState\;
                \$map_bitReader_$smStart\ <= \$map_byteReader_bitReader_$smStart\;
                \$map_byteReader_bitReader_$smDone\ <= \$map_bitReader_$smDone\;
                \$map_byteReader_bitReader_$smReturn\ <= \$map_bitReader_$smReturn\;
                \$map_bitReader_clk\ <= \$map_byteReader_bitReader_clk\;
                \$map_bitReader_rst\ <= \$map_byteReader_bitReader_rst\;
                scl_oen <= \$map_byteReader_scl_oen\;
                sda_oen <= \$map_byteReader_sda_oen\;
                \$map_byteReader_sda\ <= sda;
                \$map_byteReader_swapState\ <= \$map_cnt_zero\;

            when State_cmd_ctrl_run_22 =>
                scl_oen <= \$map_cmd_ctrl_scl_oen\;
                \$map_cmd_ctrl_scl_oen_prev\ <= '0';
                sda_oen <= \$map_cmd_ctrl_sda_oen\;
                \$map_cmd_ctrl_swapState\ <= \$map_cnt_zero\;
                \$map_cmd_ctrl_cType\ <= STOP;
                \$map_cmd_ctrl_$smStart\ <= '1';

            when State_cmd_ctrl_wait_23 =>
                scl_oen <= \$map_cmd_ctrl_scl_oen\;
                \$map_cmd_ctrl_scl_oen_prev\ <= '0';
                sda_oen <= \$map_cmd_ctrl_sda_oen\;
                \$map_cmd_ctrl_swapState\ <= \$map_cnt_zero\;
                \$map_cmd_ctrl_cType\ <= STOP;

            when State_24 =>

            when others =>

        end case;
    end process;
    process(clk)
    begin
        if RISING_EDGE(clk) then
            if rst = '1' then
                \$smReturn\ <= (others => '0');
                \$State\ <= Idle;
                Byte <= (others => '0');
                gotAck <= '0';
            else
                case \$State\ is
                    when Idle =>
                        if (\$smStart\ = '1') then
                            \$State\ <= State_cmd_ctrl_run_1;
                        end if;

                    when State_cmd_ctrl_run_1 =>
                        \$State\ <= State_cmd_ctrl_wait_2;

                    when State_cmd_ctrl_wait_2 =>
                        if (\$map_cmd_ctrl_$smDone\ = '1') then
                            \$State\ <= State_byteWriter_run_3;
                        end if;

                    when State_byteWriter_run_3 =>
                        \$State\ <= State_byteWriter_wait_4;

                    when State_byteWriter_wait_4 =>
                        if (\$map_byteWriter_$smDone\ = '1') then
                            \$State\ <= State_bitReader_run_5;
                        end if;

                    when State_bitReader_run_5 =>
                        \$State\ <= State_bitReader_wait_6;

                    when State_bitReader_wait_6 =>
                        if (\$map_bitReader_$smDone\ = '1') then
                            \$State\ <= State_7;
                            gotAck <= \$map_bitReader_$smReturn\;
                        end if;

                    when State_7 =>
                        if (gotAck = '0') then
                            \$State\ <= State_byteWriter_run_8;
                        else
                            \$State\ <= Idle;
                        end if;

                    when State_byteWriter_run_8 =>
                        \$State\ <= State_byteWriter_wait_9;

                    when State_byteWriter_wait_9 =>
                        if (\$map_byteWriter_$smDone\ = '1') then
                            \$State\ <= State_bitReader_run_10;
                        end if;

                    when State_bitReader_run_10 =>
                        \$State\ <= State_bitReader_wait_11;

                    when State_bitReader_wait_11 =>
                        if (\$map_bitReader_$smDone\ = '1') then
                            \$State\ <= State_12;
                            gotAck <= \$map_bitReader_$smReturn\;
                        end if;

                    when State_12 =>
                        if (gotAck = '0') then
                            \$State\ <= State_cmd_ctrl_run_13;
                        else
                            \$State\ <= Idle;
                        end if;

                    when State_cmd_ctrl_run_13 =>
                        \$State\ <= State_cmd_ctrl_wait_14;

                    when State_cmd_ctrl_wait_14 =>
                        if (\$map_cmd_ctrl_$smDone\ = '1') then
                            \$State\ <= State_byteWriter_run_15;
                        end if;

                    when State_byteWriter_run_15 =>
                        \$State\ <= State_byteWriter_wait_16;

                    when State_byteWriter_wait_16 =>
                        if (\$map_byteWriter_$smDone\ = '1') then
                            \$State\ <= State_bitReader_run_17;
                        end if;

                    when State_bitReader_run_17 =>
                        \$State\ <= State_bitReader_wait_18;

                    when State_bitReader_wait_18 =>
                        if (\$map_bitReader_$smDone\ = '1') then
                            \$State\ <= State_19;
                            gotAck <= \$map_bitReader_$smReturn\;
                        end if;

                    when State_19 =>
                        if (gotAck = '0') then
                            \$State\ <= State_byteReader_run_20;
                        else
                            \$State\ <= Idle;
                        end if;

                    when State_byteReader_run_20 =>
                        \$State\ <= State_byteReader_wait_21;

                    when State_byteReader_wait_21 =>
                        if (\$map_byteReader_$smDone\ = '1') then
                            \$State\ <= State_cmd_ctrl_run_22;
                            Byte <= \$map_byteReader_$smReturn\;
                        end if;

                    when State_cmd_ctrl_run_22 =>
                        \$State\ <= State_cmd_ctrl_wait_23;

                    when State_cmd_ctrl_wait_23 =>
                        if (\$map_cmd_ctrl_$smDone\ = '1') then
                            \$State\ <= State_24;
                        end if;

                    when State_24 =>
                        \$State\ <= Idle;
                        \$smReturn\ <= Byte;

                    when others =>

                end case;
            end if;
        end if;
    end process;
end;
