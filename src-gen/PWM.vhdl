library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.Casts.ALL;
use work.ShiftOps.ALL;
use work.Types.ALL;
entity PWM is
    port (
        clk : in std_logic;
        rst : in std_logic;
        value : in std_logic_vector(7 downto 0);
        newVal : in std_logic;
        LED_Out : out std_logic
    );
end;
architecture pshdlGenerated of PWM is
    signal pulseWidth : unsigned(7 downto 0);
    signal cnt : unsigned(7 downto 0);
begin
    process(cnt, pulseWidth)
    begin
        LED_Out <= '0';
        if (pulseWidth < cnt) then
            LED_Out <= '1';
        end if;
    end process;
    process(clk)
    begin
        if RISING_EDGE(clk) then
            if rst = '1' then
                pulseWidth <= (others => '0');
                cnt <= (others => '0');
            else
                if (newVal = '1') then
                    pulseWidth <= resizeNatural(bitvectorToNatural(value), 8);
                end if;
                cnt <= (cnt + 1);
            end if;
        end if;
    end process;
end;
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.Casts.ALL;
use work.ShiftOps.ALL;
use work.Types.ALL;
entity master is
    port (
        clk : in std_logic;
        rst : in std_logic;
        scl : inout std_logic;
        sda : inout std_logic;
        Tx : out std_logic;
        LED : out std_logic
    );
    constant uart_BAUDRate : unsigned(31 downto 0) := TO_UNSIGNED(9600, 32);
    constant uart_FPGAClk : unsigned(31 downto 0) := TO_UNSIGNED(66666700, 32);
    constant byteWriter_uart_BAUDRate : unsigned(31 downto 0) := TO_UNSIGNED(9600, 32);
    constant byteWriter_uart_FPGAClk : unsigned(31 downto 0) := TO_UNSIGNED(66666700, 32);
    type States is (State_0);
end;
architecture pshdlGenerated of master is
    signal sda_i : std_logic;
    signal scl_i : std_logic;
    signal sda_oen : std_logic;
    signal scl_oen : std_logic;
    signal \buffer\ : std_logic_vector(7 downto 0);
    signal I2CDataBuffered : std_logic;
    signal bufferEmpty : std_logic;
    signal i2cPrevDone : std_logic;
    signal \$map_i2c_bus_sda\ : std_logic;
    signal \$map_i2c_bus_scl\ : std_logic;
    signal \$map_i2c_bus_sda_oen\ : std_logic;
    signal \$map_i2c_bus_scl_oen\ : std_logic;
    signal \$map_i2c_bus_$smStart\ : std_logic;
    signal \$map_i2c_bus_$smDone\ : std_logic;
    signal \$map_i2c_bus_$smReturn\ : std_logic_vector(7 downto 0);
    signal \$map_i2c_bus_clk\ : std_logic;
    signal \$map_i2c_bus_rst\ : std_logic;
    signal \$map_uart_Tx\ : std_logic;
    signal \$map_uart_Rx\ : std_logic;
    signal \$map_uart_RxOut\ : std_logic;
    signal \$map_uart_TxIn\ : std_logic;
    signal \$map_uart_BAUDreset\ : std_logic;
    signal \$map_uart_BAUD\ : std_logic;
    signal \$map_uart_clk\ : std_logic;
    signal \$map_uart_rst\ : std_logic;
    signal \$map_byteWriter_uart_Tx\ : std_logic;
    signal \$map_byteWriter_uart_Rx\ : std_logic;
    signal \$map_byteWriter_uart_RxOut\ : std_logic;
    signal \$map_byteWriter_uart_TxIn\ : std_logic;
    signal \$map_byteWriter_uart_BAUDreset\ : std_logic;
    signal \$map_byteWriter_uart_BAUD\ : std_logic;
    signal \$map_byteWriter_uart_clk\ : std_logic;
    signal \$map_byteWriter_uart_rst\ : std_logic;
    signal \$map_byteWriter_byte\ : std_logic_vector(7 downto 0);
    signal \$map_byteWriter_$smStart\ : std_logic;
    signal \$map_byteWriter_$smDone\ : std_logic;
    signal \$map_byteWriter_$smReturn\ : std_logic;
    signal \$map_byteWriter_clk\ : std_logic;
    signal \$map_byteWriter_rst\ : std_logic;
    signal \$map_pwm_value\ : std_logic_vector(7 downto 0);
    signal \$map_pwm_newVal\ : std_logic;
    signal \$map_pwm_LED_Out\ : std_logic;
    signal \$map_pwm_clk\ : std_logic;
    signal \$map_pwm_rst\ : std_logic;
    signal \$State\ : States;
begin
    i2c_bus : entity work.I2C_GetPoti
        port map (
            sda => \$map_i2c_bus_sda\,
            scl => \$map_i2c_bus_scl\,
            sda_oen => \$map_i2c_bus_sda_oen\,
            scl_oen => \$map_i2c_bus_scl_oen\,
            \$smStart\ => \$map_i2c_bus_$smStart\,
            \$smDone\ => \$map_i2c_bus_$smDone\,
            \$smReturn\ => \$map_i2c_bus_$smReturn\,
            clk => \$map_i2c_bus_clk\,
            rst => \$map_i2c_bus_rst\
        );
    uart : entity work.UART
        generic map (
            BAUDRate => uart_BAUDRate,
            FPGAClk => uart_FPGAClk
        )
        port map (
            Tx => \$map_uart_Tx\,
            Rx => \$map_uart_Rx\,
            RxOut => \$map_uart_RxOut\,
            TxIn => \$map_uart_TxIn\,
            BAUDreset => \$map_uart_BAUDreset\,
            BAUD => \$map_uart_BAUD\,
            clk => \$map_uart_clk\,
            rst => \$map_uart_rst\
        );
    byteWriter : entity work.UARTWriteByte
        generic map (
            uart_BAUDRate => byteWriter_uart_BAUDRate,
            uart_FPGAClk => byteWriter_uart_FPGAClk
        )
        port map (
            uart_Tx => \$map_byteWriter_uart_Tx\,
            uart_Rx => \$map_byteWriter_uart_Rx\,
            uart_RxOut => \$map_byteWriter_uart_RxOut\,
            uart_TxIn => \$map_byteWriter_uart_TxIn\,
            uart_BAUDreset => \$map_byteWriter_uart_BAUDreset\,
            uart_BAUD => \$map_byteWriter_uart_BAUD\,
            uart_clk => \$map_byteWriter_uart_clk\,
            uart_rst => \$map_byteWriter_uart_rst\,
            byte => \$map_byteWriter_byte\,
            \$smStart\ => \$map_byteWriter_$smStart\,
            \$smDone\ => \$map_byteWriter_$smDone\,
            \$smReturn\ => \$map_byteWriter_$smReturn\,
            clk => \$map_byteWriter_clk\,
            rst => \$map_byteWriter_rst\
        );
    pwm : entity work.PWM
        port map (
            value => \$map_pwm_value\,
            newVal => \$map_pwm_newVal\,
            LED_Out => \$map_pwm_LED_Out\,
            clk => \$map_pwm_clk\,
            rst => \$map_pwm_rst\
        );
    process(\$State\, \$map_byteWriter_$smDone\, \$map_byteWriter_uart_BAUDreset\, \$map_byteWriter_uart_Rx\, \$map_byteWriter_uart_TxIn\, \$map_byteWriter_uart_clk\, \$map_byteWriter_uart_rst\, \$map_i2c_bus_$smDone\, \$map_i2c_bus_$smReturn\, \$map_i2c_bus_scl_oen\, \$map_i2c_bus_sda_oen\, \$map_pwm_LED_Out\, \$map_uart_BAUD\, \$map_uart_RxOut\, \$map_uart_Tx\, \buffer\, clk, rst, scl, scl_i, scl_oen, sda, sda_i, sda_oen)
    begin
        scl <= '0';
        sda <= '0';
        sda_i <= '0';
        scl_i <= '0';
        sda_oen <= '0';
        scl_oen <= '0';
        \$map_i2c_bus_sda\ <= '0';
        \$map_i2c_bus_scl\ <= '0';
        \$map_i2c_bus_$smStart\ <= '0';
        \$map_uart_Rx\ <= '0';
        \$map_uart_TxIn\ <= '0';
        \$map_uart_BAUDreset\ <= '0';
        \$map_byteWriter_uart_Tx\ <= '0';
        \$map_byteWriter_uart_RxOut\ <= '0';
        \$map_byteWriter_uart_BAUD\ <= '0';
        \$map_byteWriter_byte\ <= (others => '0');
        \$map_byteWriter_$smStart\ <= '0';
        \$map_pwm_newVal\ <= '0';
        if (sda = '0') then
            sda_i <= '0';
        else
            sda_i <= '1';
        end if;
        if (scl = '0') then
            scl_i <= '0';
        else
            scl_i <= '1';
        end if;
        if (not sda_oen /= '0') then
            sda <= '0';
        else
            sda <= 'Z';
        end if;
        if (not scl_oen /= '0') then
            scl <= '0';
        else
            scl <= 'Z';
        end if;
        \$map_i2c_bus_clk\ <= clk;
        \$map_i2c_bus_rst\ <= rst;
        \$map_uart_clk\ <= clk;
        \$map_uart_rst\ <= rst;
        Tx <= \$map_uart_Tx\;
        \$map_byteWriter_clk\ <= clk;
        \$map_byteWriter_rst\ <= rst;
        \$map_pwm_clk\ <= clk;
        \$map_pwm_rst\ <= rst;
        \$map_pwm_value\ <= \buffer\;
        LED <= \$map_pwm_LED_Out\;
        case \$State\ is
            when State_0 =>
                if ((\$map_i2c_bus_$smDone\ /= '0') and (\$map_byteWriter_$smDone\ /= '0')) then
                    \$map_i2c_bus_$smStart\ <= '1';
                    \$map_byteWriter_$smStart\ <= '1';
                end if;
                \$map_i2c_bus_sda\ <= sda_i;
                \$map_i2c_bus_scl\ <= scl_i;
                sda_oen <= \$map_i2c_bus_sda_oen\;
                scl_oen <= \$map_i2c_bus_scl_oen\;
                \$map_byteWriter_uart_Tx\ <= \$map_uart_Tx\;
                \$map_uart_Rx\ <= \$map_byteWriter_uart_Rx\;
                \$map_byteWriter_uart_RxOut\ <= \$map_uart_RxOut\;
                \$map_uart_TxIn\ <= \$map_byteWriter_uart_TxIn\;
                \$map_uart_BAUDreset\ <= \$map_byteWriter_uart_BAUDreset\;
                \$map_byteWriter_uart_BAUD\ <= \$map_uart_BAUD\;
                \$map_uart_clk\ <= \$map_byteWriter_uart_clk\;
                \$map_uart_rst\ <= \$map_byteWriter_uart_rst\;
                \$map_byteWriter_byte\ <= \$map_i2c_bus_$smReturn\;

            when others =>

        end case;
    end process;
    process(clk)
    begin
        if RISING_EDGE(clk) then
            if rst = '1' then
                \buffer\ <= (others => '0');
                I2CDataBuffered <= '1';
                bufferEmpty <= '1';
                i2cPrevDone <= '1';
                \$State\ <= State_0;
            else
                i2cPrevDone <= \$map_i2c_bus_$smDone\;
            end if;
        end if;
    end process;
end;
