library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.Casts.ALL;
use work.ShiftOps.ALL;
use work.Types.ALL;
use work.cmdTypePkg.all;
entity send_cmd is
    port (
        clk : in std_logic;
        rst : in std_logic;
        scl_oen : out std_logic;
        scl_oen_prev : in std_logic;
        sda_oen : out std_logic;
        swapState : in std_logic;
        cType : in cmdType;
        \$smStart\ : in std_logic;
        \$smDone\ : out std_logic;
        \$smReturn\ : out std_logic
    );
    type States is (Idle, A, B, C, D);
end;
architecture pshdlGenerated of send_cmd is
    signal \$State\ : States;
    signal scl_prev_reg : std_logic;
begin
    process(\$State\, \$smStart\, cType, scl_prev_reg, swapState)
    begin
        \$smDone\ <= '0';
        scl_oen <= '0';
        sda_oen <= '0';
        scl_oen <= '0';
        sda_oen <= '0';
        case \$State\ is
            when Idle =>
                case cType is
                    when START =>
                        scl_oen <= scl_prev_reg;
                        sda_oen <= '0';

                    when others =>

                end case;
                \$smDone\ <= '1';

            when A =>
                case cType is
                    when START =>
                        sda_oen <= '1';
                        scl_oen <= scl_prev_reg;

                    when ACK =>
                        sda_oen <= '0';
                        scl_oen <= '0';

                    when NACK =>
                        sda_oen <= '1';
                        scl_oen <= '0';

                    when STOP =>
                        scl_oen <= '1';
                        sda_oen <= '0';

                    when others =>

                end case;

            when B =>
                case cType is
                    when START =>
                        scl_oen <= '1';
                        sda_oen <= '1';

                    when STOP =>
                        scl_oen <= '1';
                        sda_oen <= '0';

                    when ACK =>
                        sda_oen <= '0';
                        scl_oen <= '1';

                    when NACK =>
                        sda_oen <= '1';
                        scl_oen <= '1';

                    when others =>

                end case;

            when C =>
                case cType is
                    when START =>
                        scl_oen <= '1';
                        sda_oen <= '0';

                    when STOP =>
                        scl_oen <= '1';
                        sda_oen <= '1';

                    when ACK =>
                        sda_oen <= '0';
                        scl_oen <= '1';

                    when NACK =>
                        sda_oen <= '1';
                        scl_oen <= '1';

                    when others =>

                end case;

            when D =>
                case cType is
                    when START =>
                        scl_oen <= '0';
                        sda_oen <= '0';

                    when STOP =>
                        scl_oen <= '0';
                        sda_oen <= '1';

                    when ACK =>
                        sda_oen <= '0';
                        scl_oen <= '0';

                    when NACK =>
                        sda_oen <= '1';
                        scl_oen <= '0';

                    when others =>

                end case;

            when others =>

        end case;
    end process;
    process(clk)
    begin
        if RISING_EDGE(clk) then
            if rst = '1' then
                \$smReturn\ <= '0';
                \$State\ <= Idle;
                scl_prev_reg <= '0';
            else
                case \$State\ is
                    when Idle =>
                        if (\$smStart\ = '1') then
                            \$State\ <= A;
                            scl_prev_reg <= scl_oen_prev;
                        end if;

                    when A =>
                        if (swapState = '1') then
                            \$State\ <= B;
                        end if;

                    when B =>
                        if (swapState = '1') then
                            \$State\ <= C;
                        end if;

                    when C =>
                        if (swapState = '1') then
                            \$State\ <= D;
                        end if;

                    when D =>
                        if (swapState = '1') then
                            \$State\ <= Idle;
                        end if;

                    when others =>

                end case;
            end if;
        end if;
    end process;
end;
