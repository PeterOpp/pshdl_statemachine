library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.Casts.ALL;
use work.ShiftOps.ALL;
use work.Types.ALL;
entity UART is
    generic (
        BAUDRate : unsigned(31 downto 0) := TO_UNSIGNED(9600, 32);
        FPGAClk : unsigned(31 downto 0) := TO_UNSIGNED(66666700, 32)
    );
    port (
        clk : in std_logic;
        rst : in std_logic;
        Tx : out std_logic;
        Rx : in std_logic;
        RxOut : out std_logic;
        TxIn : in std_logic;
        BAUDreset : in std_logic;
        BAUD : out std_logic
    );
end;
architecture pshdlGenerated of UART is
    signal \$map_baudCnt_clk_cnt\ : std_logic_vector(15 downto 0);
    signal \$map_baudCnt_zero\ : std_logic;
    signal \$map_baudCnt_clk\ : std_logic;
    signal \$map_baudCnt_rst\ : std_logic;
begin
    baudCnt : entity work.counter
        port map (
            clk_cnt => \$map_baudCnt_clk_cnt\,
            zero => \$map_baudCnt_zero\,
            clk => \$map_baudCnt_clk\,
            rst => \$map_baudCnt_rst\
        );
    process(BAUDreset, Rx, TxIn, \$map_baudCnt_zero\, clk, rst)
    begin
        \$map_baudCnt_clk\ <= clk;
        \$map_baudCnt_rst\ <= naturalToBit(ternaryOp(((rst /= '0') or (BAUDreset /= '0')), 1, 0));
        \$map_baudCnt_clk_cnt\ <= uintToBitvector(resizeUint((FPGAClk / BAUDRate), 16));
        Tx <= TxIn;
        RxOut <= Rx;
        BAUD <= \$map_baudCnt_zero\;
    end process;
end;
