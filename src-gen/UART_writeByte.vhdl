library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.Casts.ALL;
use work.ShiftOps.ALL;
use work.Types.ALL;
entity UARTWriteByte is
    generic (
        uart_BAUDRate : unsigned(31 downto 0) := TO_UNSIGNED(9600, 32);
        uart_FPGAClk : unsigned(31 downto 0) := TO_UNSIGNED(66666700, 32)
    );
    port (
        clk : in std_logic;
        rst : in std_logic;
        uart_Tx : in std_logic;
        uart_Rx : out std_logic;
        uart_RxOut : in std_logic;
        uart_TxIn : out std_logic;
        uart_BAUDreset : out std_logic;
        uart_BAUD : in std_logic;
        uart_clk : out std_logic;
        uart_rst : out std_logic;
        byte : in std_logic_vector(7 downto 0);
        \$smStart\ : in std_logic;
        \$smDone\ : out std_logic;
        \$smReturn\ : out std_logic
    );
    type States is (Idle, delay, StartBit, State_3, State_4, State_5, State_6, State_7, State_8, State_9, State_10, StopBit);
end;
architecture pshdlGenerated of UARTWriteByte is
    signal \$State\ : States;
    signal sendData : std_logic_vector(7 downto 0);
begin
    process(\$State\, \$smStart\, clk, rst, sendData, uart_BAUD)
    begin
        uart_Rx <= '0';
        uart_TxIn <= '0';
        uart_clk <= clk;
        uart_rst <= rst;
        \$smDone\ <= '0';
        uart_BAUDreset <= '0';
        case \$State\ is
            when Idle =>
                if (\$smStart\ = '1') then
                    uart_BAUDreset <= '1';
                end if;
                uart_TxIn <= '1';
                \$smDone\ <= '1';

            when delay =>

            when StartBit =>
                uart_TxIn <= '0';

            when State_3 =>
                uart_TxIn <= sendData(0);

            when State_4 =>
                uart_TxIn <= sendData(1);

            when State_5 =>
                uart_TxIn <= sendData(2);

            when State_6 =>
                uart_TxIn <= sendData(3);

            when State_7 =>
                uart_TxIn <= sendData(4);

            when State_8 =>
                uart_TxIn <= sendData(5);

            when State_9 =>
                uart_TxIn <= sendData(6);

            when State_10 =>
                uart_TxIn <= sendData(7);

            when StopBit =>
                uart_TxIn <= '1';

            when others =>

        end case;
    end process;
    process(clk)
    begin
        if RISING_EDGE(clk) then
            if rst = '1' then
                \$smReturn\ <= '0';
                \$State\ <= Idle;
                sendData <= (others => '0');
            else
                case \$State\ is
                    when Idle =>
                        if (\$smStart\ = '1') then
                            \$State\ <= delay;
                            sendData <= byte;
                        end if;

                    when delay =>
                        \$State\ <= StartBit;

                    when StartBit =>
                        if (uart_BAUD = '1') then
                            \$State\ <= State_3;
                        end if;

                    when State_3 =>
                        if (uart_BAUD = '1') then
                            \$State\ <= State_4;
                        end if;

                    when State_4 =>
                        if (uart_BAUD = '1') then
                            \$State\ <= State_5;
                        end if;

                    when State_5 =>
                        if (uart_BAUD = '1') then
                            \$State\ <= State_6;
                        end if;

                    when State_6 =>
                        if (uart_BAUD = '1') then
                            \$State\ <= State_7;
                        end if;

                    when State_7 =>
                        if (uart_BAUD = '1') then
                            \$State\ <= State_8;
                        end if;

                    when State_8 =>
                        if (uart_BAUD = '1') then
                            \$State\ <= State_9;
                        end if;

                    when State_9 =>
                        if (uart_BAUD = '1') then
                            \$State\ <= State_10;
                        end if;

                    when State_10 =>
                        if (uart_BAUD = '1') then
                            \$State\ <= StopBit;
                        end if;

                    when StopBit =>
                        if (uart_BAUD = '1') then
                            \$State\ <= Idle;
                        end if;

                    when others =>

                end case;
            end if;
        end if;
    end process;
end;
