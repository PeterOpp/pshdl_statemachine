library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.Casts.ALL;
use work.ShiftOps.ALL;
use work.Types.ALL;
entity readBit is
    port (
        clk : in std_logic;
        rst : in std_logic;
        sda_oen : out std_logic;
        scl_oen : out std_logic;
        sda : in std_logic;
        swapState : in std_logic;
        \$smStart\ : in std_logic;
        \$smDone\ : out std_logic;
        \$smReturn\ : out std_logic
    );
    type States is (Idle, State_1, State_2, State_3, State_4);
end;
architecture pshdlGenerated of readBit is
    signal \$State\ : States;
    signal res : std_logic;
begin
    process(\$State\, \$smStart\, swapState)
    begin
        \$smDone\ <= '0';
        sda_oen <= '0';
        scl_oen <= '0';
        case \$State\ is
            when Idle =>
                sda_oen <= '0';
                scl_oen <= '0';
                \$smDone\ <= '1';

            when State_1 =>
                scl_oen <= '0';
                sda_oen <= '1';

            when State_2 =>
                scl_oen <= '1';
                sda_oen <= '1';

            when State_3 =>
                scl_oen <= '1';
                sda_oen <= '1';

            when State_4 =>
                scl_oen <= '0';
                sda_oen <= '1';

            when others =>

        end case;
    end process;
    process(clk)
    begin
        if RISING_EDGE(clk) then
            if rst = '1' then
                \$smReturn\ <= '0';
                \$State\ <= Idle;
                res <= '0';
            else
                case \$State\ is
                    when Idle =>
                        if (\$smStart\ = '1') then
                            \$State\ <= State_1;
                        end if;

                    when State_1 =>
                        if (swapState = '1') then
                            \$State\ <= State_2;
                        end if;

                    when State_2 =>
                        if (swapState = '1') then
                            \$State\ <= State_3;
                            res <= sda;
                        end if;

                    when State_3 =>
                        if (swapState = '1') then
                            \$State\ <= State_4;
                        end if;

                    when State_4 =>
                        if (swapState = '1') then
                            \$State\ <= Idle;
                            \$smReturn\ <= res;
                        end if;

                    when others =>

                end case;
            end if;
        end if;
    end process;
end;
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.Casts.ALL;
use work.ShiftOps.ALL;
use work.Types.ALL;
entity readByte is
    port (
        clk : in std_logic;
        rst : in std_logic;
        bitReader_sda_oen : in std_logic;
        bitReader_scl_oen : in std_logic;
        bitReader_sda : out std_logic;
        bitReader_swapState : out std_logic;
        \bitReader_$smStart\ : out std_logic;
        \bitReader_$smDone\ : in std_logic;
        \bitReader_$smReturn\ : in std_logic;
        bitReader_clk : out std_logic;
        bitReader_rst : out std_logic;
        scl_oen : out std_logic;
        sda_oen : out std_logic;
        sda : in std_logic;
        swapState : in std_logic;
        \$smStart\ : in std_logic;
        \$smDone\ : out std_logic;
        \$smReturn\ : out std_logic_vector(7 downto 0)
    );
    type States is (Idle, State_1, State_bitReader_wait_2, State_3, State_bitReader_wait_4, State_5, State_bitReader_wait_6, State_7, State_bitReader_wait_8, State_9, State_bitReader_wait_10, State_11, State_bitReader_wait_12, State_13, State_bitReader_wait_14, State_15, State_bitReader_wait_16, State_17);
end;
architecture pshdlGenerated of readByte is
    signal \$State\ : States;
    signal res : std_logic_vector(7 downto 0);
begin
    process(\$State\, \$smStart\, \bitReader_$smDone\, bitReader_scl_oen, bitReader_sda_oen, clk, rst, sda, swapState)
    begin
        bitReader_sda <= '0';
        bitReader_swapState <= '0';
        bitReader_clk <= clk;
        bitReader_rst <= rst;
        \$smDone\ <= '0';
        scl_oen <= '0';
        sda_oen <= '0';
        \bitReader_$smStart\ <= '0';
        case \$State\ is
            when Idle =>
                sda_oen <= '0';
                scl_oen <= '0';
                \$smDone\ <= '1';

            when State_1 =>
                sda_oen <= bitReader_sda_oen;
                scl_oen <= bitReader_scl_oen;
                bitReader_sda <= sda;
                bitReader_swapState <= swapState;
                \bitReader_$smStart\ <= '1';

            when State_bitReader_wait_2 =>
                sda_oen <= bitReader_sda_oen;
                scl_oen <= bitReader_scl_oen;
                bitReader_sda <= sda;
                bitReader_swapState <= swapState;

            when State_3 =>
                sda_oen <= bitReader_sda_oen;
                scl_oen <= bitReader_scl_oen;
                bitReader_sda <= sda;
                bitReader_swapState <= swapState;
                \bitReader_$smStart\ <= '1';

            when State_bitReader_wait_4 =>
                sda_oen <= bitReader_sda_oen;
                scl_oen <= bitReader_scl_oen;
                bitReader_sda <= sda;
                bitReader_swapState <= swapState;

            when State_5 =>
                sda_oen <= bitReader_sda_oen;
                scl_oen <= bitReader_scl_oen;
                bitReader_sda <= sda;
                bitReader_swapState <= swapState;
                \bitReader_$smStart\ <= '1';

            when State_bitReader_wait_6 =>
                sda_oen <= bitReader_sda_oen;
                scl_oen <= bitReader_scl_oen;
                bitReader_sda <= sda;
                bitReader_swapState <= swapState;

            when State_7 =>
                sda_oen <= bitReader_sda_oen;
                scl_oen <= bitReader_scl_oen;
                bitReader_sda <= sda;
                bitReader_swapState <= swapState;
                \bitReader_$smStart\ <= '1';

            when State_bitReader_wait_8 =>
                sda_oen <= bitReader_sda_oen;
                scl_oen <= bitReader_scl_oen;
                bitReader_sda <= sda;
                bitReader_swapState <= swapState;

            when State_9 =>
                sda_oen <= bitReader_sda_oen;
                scl_oen <= bitReader_scl_oen;
                bitReader_sda <= sda;
                bitReader_swapState <= swapState;
                \bitReader_$smStart\ <= '1';

            when State_bitReader_wait_10 =>
                sda_oen <= bitReader_sda_oen;
                scl_oen <= bitReader_scl_oen;
                bitReader_sda <= sda;
                bitReader_swapState <= swapState;

            when State_11 =>
                sda_oen <= bitReader_sda_oen;
                scl_oen <= bitReader_scl_oen;
                bitReader_sda <= sda;
                bitReader_swapState <= swapState;
                \bitReader_$smStart\ <= '1';

            when State_bitReader_wait_12 =>
                sda_oen <= bitReader_sda_oen;
                scl_oen <= bitReader_scl_oen;
                bitReader_sda <= sda;
                bitReader_swapState <= swapState;

            when State_13 =>
                sda_oen <= bitReader_sda_oen;
                scl_oen <= bitReader_scl_oen;
                bitReader_sda <= sda;
                bitReader_swapState <= swapState;
                \bitReader_$smStart\ <= '1';

            when State_bitReader_wait_14 =>
                sda_oen <= bitReader_sda_oen;
                scl_oen <= bitReader_scl_oen;
                bitReader_sda <= sda;
                bitReader_swapState <= swapState;

            when State_15 =>
                sda_oen <= bitReader_sda_oen;
                scl_oen <= bitReader_scl_oen;
                bitReader_sda <= sda;
                bitReader_swapState <= swapState;
                \bitReader_$smStart\ <= '1';

            when State_bitReader_wait_16 =>
                sda_oen <= bitReader_sda_oen;
                scl_oen <= bitReader_scl_oen;
                bitReader_sda <= sda;
                bitReader_swapState <= swapState;

            when State_17 =>

            when others =>

        end case;
    end process;
    process(clk)
    begin
        if RISING_EDGE(clk) then
            if rst = '1' then
                \$smReturn\ <= (others => '0');
                \$State\ <= Idle;
                res <= (others => '0');
            else
                case \$State\ is
                    when Idle =>
                        if (\$smStart\ = '1') then
                            \$State\ <= State_1;
                        end if;

                    when State_1 =>
                        \$State\ <= State_bitReader_wait_2;

                    when State_bitReader_wait_2 =>
                        if (\bitReader_$smDone\ = '1') then
                            \$State\ <= State_3;
                            res(7) <= \bitReader_$smReturn\;
                        end if;

                    when State_3 =>
                        \$State\ <= State_bitReader_wait_4;

                    when State_bitReader_wait_4 =>
                        if (\bitReader_$smDone\ = '1') then
                            \$State\ <= State_5;
                            res(6) <= \bitReader_$smReturn\;
                        end if;

                    when State_5 =>
                        \$State\ <= State_bitReader_wait_6;

                    when State_bitReader_wait_6 =>
                        if (\bitReader_$smDone\ = '1') then
                            \$State\ <= State_7;
                            res(5) <= \bitReader_$smReturn\;
                        end if;

                    when State_7 =>
                        \$State\ <= State_bitReader_wait_8;

                    when State_bitReader_wait_8 =>
                        if (\bitReader_$smDone\ = '1') then
                            \$State\ <= State_9;
                            res(4) <= \bitReader_$smReturn\;
                        end if;

                    when State_9 =>
                        \$State\ <= State_bitReader_wait_10;

                    when State_bitReader_wait_10 =>
                        if (\bitReader_$smDone\ = '1') then
                            \$State\ <= State_11;
                            res(3) <= \bitReader_$smReturn\;
                        end if;

                    when State_11 =>
                        \$State\ <= State_bitReader_wait_12;

                    when State_bitReader_wait_12 =>
                        if (\bitReader_$smDone\ = '1') then
                            \$State\ <= State_13;
                            res(2) <= \bitReader_$smReturn\;
                        end if;

                    when State_13 =>
                        \$State\ <= State_bitReader_wait_14;

                    when State_bitReader_wait_14 =>
                        if (\bitReader_$smDone\ = '1') then
                            \$State\ <= State_15;
                            res(1) <= \bitReader_$smReturn\;
                        end if;

                    when State_15 =>
                        \$State\ <= State_bitReader_wait_16;

                    when State_bitReader_wait_16 =>
                        if (\bitReader_$smDone\ = '1') then
                            \$State\ <= State_17;
                            res(0) <= \bitReader_$smReturn\;
                        end if;

                    when State_17 =>
                        \$State\ <= Idle;
                        \$smReturn\ <= res;

                    when others =>

                end case;
            end if;
        end if;
    end process;
end;
