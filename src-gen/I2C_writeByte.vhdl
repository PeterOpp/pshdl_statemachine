library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.Casts.ALL;
use work.ShiftOps.ALL;
use work.Types.ALL;
entity writeBit is
    port (
        clk : in std_logic;
        rst : in std_logic;
        data : in std_logic;
        swapState : in std_logic;
        scl_oen : out std_logic;
        sda_oen : out std_logic;
        \$smStart\ : in std_logic;
        \$smDone\ : out std_logic;
        \$smReturn\ : out std_logic
    );
    type States is (Idle, A, B, C, D);
end;
architecture pshdlGenerated of writeBit is
    signal \$State\ : States;
begin
    process(\$State\, \$smStart\, data, swapState)
    begin
        \$smDone\ <= '0';
        scl_oen <= '0';
        sda_oen <= '0';
        case \$State\ is
            when Idle =>
                sda_oen <= '0';
                scl_oen <= '0';
                \$smDone\ <= '1';

            when A =>
                scl_oen <= '0';
                sda_oen <= data;

            when B =>
                scl_oen <= '1';
                sda_oen <= data;

            when C =>
                scl_oen <= '1';
                sda_oen <= data;

            when D =>
                scl_oen <= '0';
                sda_oen <= data;

            when others =>

        end case;
    end process;
    process(clk)
    begin
        if RISING_EDGE(clk) then
            if rst = '1' then
                \$smReturn\ <= '0';
                \$State\ <= Idle;
            else
                case \$State\ is
                    when Idle =>
                        if (\$smStart\ = '1') then
                            \$State\ <= A;
                        end if;

                    when A =>
                        if (swapState = '1') then
                            \$State\ <= B;
                        end if;

                    when B =>
                        if (swapState = '1') then
                            \$State\ <= C;
                        end if;

                    when C =>
                        if (swapState = '1') then
                            \$State\ <= D;
                        end if;

                    when D =>
                        if (swapState = '1') then
                            \$State\ <= Idle;
                        end if;

                    when others =>

                end case;
            end if;
        end if;
    end process;
end;
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.Casts.ALL;
use work.ShiftOps.ALL;
use work.Types.ALL;
entity writeByte is
    port (
        clk : in std_logic;
        rst : in std_logic;
        byte : in std_logic_vector(7 downto 0);
        scl_oen : out std_logic;
        sda_oen : out std_logic;
        swapState : in std_logic;
        \$smStart\ : in std_logic;
        \$smDone\ : out std_logic;
        \$smReturn\ : out std_logic
    );
    type States is (Idle, State_1, State_bitWriter_wait_2, State_3, State_bitWriter_wait_4, State_5, State_bitWriter_wait_6, State_7, State_bitWriter_wait_8, State_9, State_bitWriter_wait_10, State_11, State_bitWriter_wait_12, State_13, State_bitWriter_wait_14, State_15, State_bitWriter_wait_16);
end;
architecture pshdlGenerated of writeByte is
    signal \$State\ : States;
    signal \$map_bitWriter_data\ : std_logic;
    signal \$map_bitWriter_swapState\ : std_logic;
    signal \$map_bitWriter_scl_oen\ : std_logic;
    signal \$map_bitWriter_sda_oen\ : std_logic;
    signal \$map_bitWriter_$smStart\ : std_logic;
    signal \$map_bitWriter_$smDone\ : std_logic;
    signal \$map_bitWriter_$smReturn\ : std_logic;
    signal \$map_bitWriter_clk\ : std_logic;
    signal \$map_bitWriter_rst\ : std_logic;
begin
    bitWriter : entity work.writeBit
        port map (
            data => \$map_bitWriter_data\,
            swapState => \$map_bitWriter_swapState\,
            scl_oen => \$map_bitWriter_scl_oen\,
            sda_oen => \$map_bitWriter_sda_oen\,
            \$smStart\ => \$map_bitWriter_$smStart\,
            \$smDone\ => \$map_bitWriter_$smDone\,
            \$smReturn\ => \$map_bitWriter_$smReturn\,
            clk => \$map_bitWriter_clk\,
            rst => \$map_bitWriter_rst\
        );
    process(\$State\, \$map_bitWriter_$smDone\, \$map_bitWriter_scl_oen\, \$map_bitWriter_sda_oen\, \$smStart\, byte, clk, rst, swapState)
    begin
        \$smDone\ <= '0';
        \$map_bitWriter_data\ <= '0';
        \$map_bitWriter_swapState\ <= '0';
        scl_oen <= '0';
        sda_oen <= '0';
        \$map_bitWriter_clk\ <= clk;
        \$map_bitWriter_rst\ <= rst;
        \$map_bitWriter_$smStart\ <= '0';
        case \$State\ is
            when Idle =>
                sda_oen <= '0';
                scl_oen <= '0';
                \$smDone\ <= '1';

            when State_1 =>
                \$map_bitWriter_data\ <= byte(7);
                \$map_bitWriter_swapState\ <= swapState;
                scl_oen <= \$map_bitWriter_scl_oen\;
                sda_oen <= \$map_bitWriter_sda_oen\;
                \$map_bitWriter_$smStart\ <= '1';

            when State_bitWriter_wait_2 =>
                \$map_bitWriter_data\ <= byte(7);
                \$map_bitWriter_swapState\ <= swapState;
                scl_oen <= \$map_bitWriter_scl_oen\;
                sda_oen <= \$map_bitWriter_sda_oen\;

            when State_3 =>
                \$map_bitWriter_data\ <= byte(6);
                \$map_bitWriter_swapState\ <= swapState;
                scl_oen <= \$map_bitWriter_scl_oen\;
                sda_oen <= \$map_bitWriter_sda_oen\;
                \$map_bitWriter_$smStart\ <= '1';

            when State_bitWriter_wait_4 =>
                \$map_bitWriter_data\ <= byte(6);
                \$map_bitWriter_swapState\ <= swapState;
                scl_oen <= \$map_bitWriter_scl_oen\;
                sda_oen <= \$map_bitWriter_sda_oen\;

            when State_5 =>
                \$map_bitWriter_data\ <= byte(5);
                \$map_bitWriter_swapState\ <= swapState;
                scl_oen <= \$map_bitWriter_scl_oen\;
                sda_oen <= \$map_bitWriter_sda_oen\;
                \$map_bitWriter_$smStart\ <= '1';

            when State_bitWriter_wait_6 =>
                \$map_bitWriter_data\ <= byte(5);
                \$map_bitWriter_swapState\ <= swapState;
                scl_oen <= \$map_bitWriter_scl_oen\;
                sda_oen <= \$map_bitWriter_sda_oen\;

            when State_7 =>
                \$map_bitWriter_data\ <= byte(4);
                \$map_bitWriter_swapState\ <= swapState;
                scl_oen <= \$map_bitWriter_scl_oen\;
                sda_oen <= \$map_bitWriter_sda_oen\;
                \$map_bitWriter_$smStart\ <= '1';

            when State_bitWriter_wait_8 =>
                \$map_bitWriter_data\ <= byte(4);
                \$map_bitWriter_swapState\ <= swapState;
                scl_oen <= \$map_bitWriter_scl_oen\;
                sda_oen <= \$map_bitWriter_sda_oen\;

            when State_9 =>
                \$map_bitWriter_data\ <= byte(3);
                \$map_bitWriter_swapState\ <= swapState;
                scl_oen <= \$map_bitWriter_scl_oen\;
                sda_oen <= \$map_bitWriter_sda_oen\;
                \$map_bitWriter_$smStart\ <= '1';

            when State_bitWriter_wait_10 =>
                \$map_bitWriter_data\ <= byte(3);
                \$map_bitWriter_swapState\ <= swapState;
                scl_oen <= \$map_bitWriter_scl_oen\;
                sda_oen <= \$map_bitWriter_sda_oen\;

            when State_11 =>
                \$map_bitWriter_data\ <= byte(2);
                \$map_bitWriter_swapState\ <= swapState;
                scl_oen <= \$map_bitWriter_scl_oen\;
                sda_oen <= \$map_bitWriter_sda_oen\;
                \$map_bitWriter_$smStart\ <= '1';

            when State_bitWriter_wait_12 =>
                \$map_bitWriter_data\ <= byte(2);
                \$map_bitWriter_swapState\ <= swapState;
                scl_oen <= \$map_bitWriter_scl_oen\;
                sda_oen <= \$map_bitWriter_sda_oen\;

            when State_13 =>
                \$map_bitWriter_data\ <= byte(1);
                \$map_bitWriter_swapState\ <= swapState;
                scl_oen <= \$map_bitWriter_scl_oen\;
                sda_oen <= \$map_bitWriter_sda_oen\;
                \$map_bitWriter_$smStart\ <= '1';

            when State_bitWriter_wait_14 =>
                \$map_bitWriter_data\ <= byte(1);
                \$map_bitWriter_swapState\ <= swapState;
                scl_oen <= \$map_bitWriter_scl_oen\;
                sda_oen <= \$map_bitWriter_sda_oen\;

            when State_15 =>
                \$map_bitWriter_data\ <= byte(0);
                \$map_bitWriter_swapState\ <= swapState;
                scl_oen <= \$map_bitWriter_scl_oen\;
                sda_oen <= \$map_bitWriter_sda_oen\;
                \$map_bitWriter_$smStart\ <= '1';

            when State_bitWriter_wait_16 =>
                \$map_bitWriter_data\ <= byte(0);
                \$map_bitWriter_swapState\ <= swapState;
                scl_oen <= \$map_bitWriter_scl_oen\;
                sda_oen <= \$map_bitWriter_sda_oen\;

            when others =>

        end case;
    end process;
    process(clk)
    begin
        if RISING_EDGE(clk) then
            if rst = '1' then
                \$smReturn\ <= '0';
                \$State\ <= Idle;
            else
                case \$State\ is
                    when Idle =>
                        if (\$smStart\ = '1') then
                            \$State\ <= State_1;
                        end if;

                    when State_1 =>
                        \$State\ <= State_bitWriter_wait_2;

                    when State_bitWriter_wait_2 =>
                        if (\$map_bitWriter_$smDone\ = '1') then
                            \$State\ <= State_3;
                        end if;

                    when State_3 =>
                        \$State\ <= State_bitWriter_wait_4;

                    when State_bitWriter_wait_4 =>
                        if (\$map_bitWriter_$smDone\ = '1') then
                            \$State\ <= State_5;
                        end if;

                    when State_5 =>
                        \$State\ <= State_bitWriter_wait_6;

                    when State_bitWriter_wait_6 =>
                        if (\$map_bitWriter_$smDone\ = '1') then
                            \$State\ <= State_7;
                        end if;

                    when State_7 =>
                        \$State\ <= State_bitWriter_wait_8;

                    when State_bitWriter_wait_8 =>
                        if (\$map_bitWriter_$smDone\ = '1') then
                            \$State\ <= State_9;
                        end if;

                    when State_9 =>
                        \$State\ <= State_bitWriter_wait_10;

                    when State_bitWriter_wait_10 =>
                        if (\$map_bitWriter_$smDone\ = '1') then
                            \$State\ <= State_11;
                        end if;

                    when State_11 =>
                        \$State\ <= State_bitWriter_wait_12;

                    when State_bitWriter_wait_12 =>
                        if (\$map_bitWriter_$smDone\ = '1') then
                            \$State\ <= State_13;
                        end if;

                    when State_13 =>
                        \$State\ <= State_bitWriter_wait_14;

                    when State_bitWriter_wait_14 =>
                        if (\$map_bitWriter_$smDone\ = '1') then
                            \$State\ <= State_15;
                        end if;

                    when State_15 =>
                        \$State\ <= State_bitWriter_wait_16;

                    when State_bitWriter_wait_16 =>
                        if (\$map_bitWriter_$smDone\ = '1') then
                            \$State\ <= Idle;
                        end if;

                    when others =>

                end case;
            end if;
        end if;
    end process;
end;
